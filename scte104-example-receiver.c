#include <gst/gst.h>
#include <gst-scte-104.h>

#define MAKE_AND_ADD(var, pipe, name, label)                                   \
  G_STMT_START                                                                 \
  {                                                                            \
    GError* make_and_add_err = NULL;                                           \
    if (G_UNLIKELY(!(var = (gst_parse_bin_from_description_full(               \
                       name,                                                   \
                       TRUE,                                                   \
                       NULL,                                                   \
                       GST_PARSE_FLAG_NO_SINGLE_ELEMENT_BINS,                  \
                       &make_and_add_err))))) {                                \
      GST_ERROR(                                                               \
        "Could not create element %s (%s)", name, make_and_add_err->message);  \
      g_clear_error(&make_and_add_err);                                        \
      goto label;                                                              \
    }                                                                          \
    if (G_UNLIKELY(!gst_bin_add(GST_BIN_CAST(pipe), var))) {                   \
      GST_ERROR("Could not add element %s", name);                             \
      goto label;                                                              \
    }                                                                          \
  }                                                                            \
  G_STMT_END

static GstPadProbeReturn
scte_104_probe_cb (GstPad * pad, GstPadProbeInfo * info, gpointer data)
{
  GstSCTE104PacketParser *parser = (GstSCTE104PacketParser *) data;
  gpointer state = NULL;
  GstBuffer *buf = GST_PAD_PROBE_INFO_BUFFER (info);
  GstMapInfo map;
  GstSCTE104Meta *scte_104_meta;

  gst_buffer_map (buf, &map, GST_MAP_READ);
  while ((scte_104_meta =
          (GstSCTE104Meta *) gst_buffer_iterate_meta_filtered (buf, &state,
              GST_SCTE_104_META_API_TYPE)) != NULL) {
    GstSCTE104Packet scte_104_pkt = { 0, };

    if (gst_scte_104_packet_parser_parse (parser, &scte_104_pkt,
            scte_104_meta->data, scte_104_meta->size)) {
      guint i;

      g_print
          ("One SCTE 104 packet received with single opID %04x, num ops: %u\n",
          scte_104_pkt.so_msg.op_id, scte_104_pkt.mo_msg.num_ops);

      for (i = 0; i < scte_104_pkt.mo_msg.num_ops; i++) {
        g_print
            ("Multiple operation message contains operation with opID %04x\n",
            scte_104_pkt.mo_msg.ops[i].op_id);
      }

      gst_scte_104_packet_unset (&scte_104_pkt);
    }
  }
  gst_buffer_unmap (buf, &map);

  return GST_PAD_PROBE_OK;
}

static void
pad_added_cb (GstElement * sink, GstPad * srcpad, GstPad * sinkpad)
{
  g_assert (gst_pad_link (srcpad, sinkpad) == GST_PAD_LINK_OK);
  g_signal_handlers_disconnect_by_func (sink, pad_added_cb, sinkpad);
  gst_object_unref (sinkpad);
}

int
main (int ac, char **av)
{
  int ret = 0;
  GstElement *pipe;
  GstBus *bus;
  GstElement *vsrc, *vsink;
  GstPad *pad;
  GstSCTE104PacketParser *parser = gst_scte_104_packet_parser_new ();

  gst_init (NULL, NULL);

  pipe = gst_pipeline_new (NULL);

  MAKE_AND_ADD (vsrc, pipe, "awscdisrc", err);
  MAKE_AND_ADD (vsink, pipe, "fakesink", err);

  pad = gst_element_get_static_pad (vsink, "sink");
  g_signal_connect (vsrc, "pad-added", G_CALLBACK (pad_added_cb), pad);
  gst_pad_add_probe (pad, GST_PAD_PROBE_TYPE_BUFFER, scte_104_probe_cb, parser,
      NULL);

  gst_element_set_state (pipe, GST_STATE_PLAYING);

  bus = gst_pipeline_get_bus (GST_PIPELINE (pipe));

  gst_message_unref (gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE,
          GST_MESSAGE_EOS));

  gst_object_unref (bus);

done:
  gst_element_set_state (pipe, GST_STATE_NULL);
  gst_object_unref (pipe);
  gst_scte_104_packet_parser_free (parser);
  return ret;

err:
  ret = 1;
  goto done;
}
