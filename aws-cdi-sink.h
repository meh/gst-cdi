/* GStreamer
 *
 * Copyright (C) 2022 Mathieu Duponchelle <mathieu@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_AWS_CDI_SINK_H__
#define __GST_AWS_CDI_SINK_H__

#include <gst/gst.h>
#include <gst/base/base.h>
#include <cdi_core_api.h>
#include <cdi_baseline_profile_02_00_api.h>

G_BEGIN_DECLS

#define GST_TYPE_AWS_CDI_CONNECTION  (gst_aws_cdi_connection_get_type ())

G_DECLARE_FINAL_TYPE (GstAwsCdiConnection, gst_aws_cdi_connection, GST,
    AWS_CDI_CONNECTION, GObject)

#define GST_TYPE_AWS_CDI_CONNECTIONS  (gst_aws_cdi_connections_get_type ())

G_DECLARE_FINAL_TYPE (GstAwsCdiConnections, gst_aws_cdi_connections, GST,
    AWS_CDI_CONNECTIONS, GObject)

#define GST_TYPE_AWS_CDI_SINK \
  (gst_aws_cdi_sink_get_type())
#define GST_AWS_CDI_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_AWS_CDI_SINK, GstAwsCdiSink))
#define GST_AWS_CDI_SINK_CAST(obj) \
  ((GstAwsCdiSink*)obj)
#define GST_AWS_CDI_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_AWS_CDI_SINK, GstAwsCdiSinkClass))
#define GST_IS_AWS_CDI_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_AWS_CDI_SINK))
#define GST_IS_AWS_CDI_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_AWS_CDI_SINK))

typedef struct _GstAwsCdiSink GstAwsCdiSink;
typedef struct _GstAwsCdiSinkClass GstAwsCdiSinkClass;

struct _GstAwsCdiSink
{
  GstBaseSink parent;

  GCond cond;
  CdiConnectionStatus connection_status;
  gboolean rendered;
  gsize adapter_size;
  gpointer adapter_tx_buffer_ptr;
  guint64 adapter_offset;
  guint16 stream_id;
  gchar * language_code;
  GstCaps *caps;

  gboolean unlock;

  CdiAvmConfig av_config;
  gboolean send_av_config;
  gboolean send_meta_config;

  /* Protected by object lock */
  GstAwsCdiConnections *connections;
  GstAwsCdiConnection *connection;

  /* Properties */
  gint dest_port;
  gchar *dest_address;
  gchar *src_address;
};

struct _GstAwsCdiSinkClass
{
  GstBaseSinkClass parent_class;
};

GType gst_aws_cdi_sink_get_type (void);

GST_ELEMENT_REGISTER_DECLARE (aws_cdi_sink);

G_END_DECLS

#endif /* __GST_AWS_CDI_SINK_H__ */
