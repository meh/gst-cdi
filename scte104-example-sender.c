#include <gst/gst.h>
#include <gst-scte-104.h>

#define MAKE_AND_ADD(var, pipe, name, label)                                   \
  G_STMT_START                                                                 \
  {                                                                            \
    GError* make_and_add_err = NULL;                                           \
    if (G_UNLIKELY(!(var = (gst_parse_bin_from_description_full(               \
                       name,                                                   \
                       TRUE,                                                   \
                       NULL,                                                   \
                       GST_PARSE_FLAG_NO_SINGLE_ELEMENT_BINS,                  \
                       &make_and_add_err))))) {                                \
      GST_ERROR(                                                               \
        "Could not create element %s (%s)", name, make_and_add_err->message);  \
      g_clear_error(&make_and_add_err);                                        \
      goto label;                                                              \
    }                                                                          \
    if (G_UNLIKELY(!gst_bin_add(GST_BIN_CAST(pipe), var))) {                   \
      GST_ERROR("Could not add element %s", name);                             \
      goto label;                                                              \
    }                                                                          \
  }                                                                            \
  G_STMT_END

static GstPadProbeReturn
add_scte_104_probe_cb (GstPad * pad, GstPadProbeInfo * info, gpointer data)
{
  GstBuffer *buf = GST_PAD_PROBE_INFO_BUFFER (info);
  GstSCTE104Packet scte_104_pkt = { 0, };
  GstSCTE104MultipleOperationOperation *op;
  guint8 *scte_104_bytes = NULL;
  guint16 n_bytes;

  gst_scte_104_packet_init (&scte_104_pkt, 0xffff);

  if (!gst_scte_104_packet_add_multiple_operation_operation (&scte_104_pkt,
          MO_SPLICE_REQUEST_DATA, &op)) {
    GST_ERROR ("failed to add operation!");
    goto done;
  }

  op->sr_data.splice_insert_type = 0x02;
  op->sr_data.splice_event_id = 0x1234;
  op->sr_data.unique_program_id = 0x4567;
  op->sr_data.pre_roll_time = 0;
  op->sr_data.brk_duration = 300;
  op->sr_data.avail_num = 1;
  op->sr_data.avails_expected = 2;
  op->sr_data.auto_return_flag = 1;

  if (!gst_scte_104_packet_to_bytes (&scte_104_pkt, &scte_104_bytes, &n_bytes)) {
    GST_ERROR ("Failed to dump SCTE 104 message to bytes");
    goto done;
  }

  if (n_bytes > 254) {
    GST_ERROR ("Fragmented SCTE messages are not supported");
    free (scte_104_bytes);
    goto done;
  }

  buf = gst_buffer_make_writable (buf);

  gst_buffer_add_scte_104_meta (buf, scte_104_bytes, n_bytes);

  GST_PAD_PROBE_INFO_DATA (info) = buf;

done:
  return GST_PAD_PROBE_OK;
}

int
main (int ac, char **av)
{
  int ret = 0;
  GstElement *pipe;
  GstBus *bus;
  GstElement *vsrc, *vqueue, *vsink;
  GstPad *pad;

  gst_init (NULL, NULL);

  pipe = gst_pipeline_new (NULL);

  MAKE_AND_ADD (vsrc, pipe, "videotestsrc", err);
  MAKE_AND_ADD (vqueue, pipe, "queue", err);
  MAKE_AND_ADD (vsink, pipe, "awscdisink", err);

  if (!gst_element_link_many (vsrc, vqueue, vsink, NULL)) {
    GST_ERROR ("Failed to link pipeline");
    goto err;
  }

  pad = gst_element_get_static_pad (vsrc, "src");
  gst_pad_add_probe (pad, GST_PAD_PROBE_TYPE_BUFFER, add_scte_104_probe_cb,
      NULL, NULL);
  gst_object_unref (pad);

  gst_element_set_state (pipe, GST_STATE_PLAYING);

  bus = gst_pipeline_get_bus (GST_PIPELINE (pipe));

  gst_message_unref (gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE,
          GST_MESSAGE_EOS));

  gst_object_unref (bus);

done:
  gst_element_set_state (pipe, GST_STATE_NULL);
  gst_object_unref (pipe);
  return ret;

err:
  ret = 1;
  goto done;
}
