/* GStreamer
 * Copyright (C) 2022 Mathieu Duponchelle <mathieu@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cdi_log_api.h>
#include <cdi_avm_payloads_api.h>
#include <gst/video/video.h>
#include <gst/audio/audio.h>
#include <gst-scte-104.h>

#include "aws-cdi-sink.h"
#include "common.h"

GST_DEBUG_CATEGORY_STATIC (gst_aws_cdi_sink_debug);
#define GST_CAT_DEFAULT gst_aws_cdi_sink_debug

struct _GstAwsCdiConnection
{
  GObject parent;
  guint reservations;
  guint endpoints;
  guint64 adapter_size;
  gchar *src_address;
  gchar *dest_address;
  gint dest_port;
  CdiConnectionHandle connection_handle;
  CdiAdapterHandle adapter_handle;
  GMutex lock;
};

enum
{
  CREATED,
  STATUS_CHANGED,
  LAST_SIGNAL
};

G_DEFINE_TYPE (GstAwsCdiConnection, gst_aws_cdi_connection, G_TYPE_OBJECT);

static guint connection_signals[LAST_SIGNAL] = { 0 };

static void
gst_aws_cdi_connection_finalize (GObject * object)
{
  GstAwsCdiConnection *self = GST_AWS_CDI_CONNECTION (object);

  g_mutex_clear (&self->lock);
  g_free (self->dest_address);
  g_free (self->src_address);

  if (self->connection_handle) {
    GST_INFO_OBJECT (self, "Closing connection %p", self->connection_handle);
    CdiCoreConnectionDestroy (self->connection_handle);
    self->connection_handle = NULL;
  }

  if (self->adapter_handle) {
    GST_INFO_OBJECT (self, "Destroying adapter %p", self->adapter_handle);
    CdiCoreNetworkAdapterDestroy (self->adapter_handle);
    self->adapter_handle = NULL;
  }

  G_OBJECT_CLASS (gst_aws_cdi_connection_parent_class)->finalize (object);
}

static void
gst_aws_cdi_connection_class_init (GstAwsCdiConnectionClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->finalize = gst_aws_cdi_connection_finalize;

  // Args: CdiConnectionHandle, adapter_tx_buffer_ptr
  connection_signals[CREATED] =
      g_signal_new ("created", G_TYPE_FROM_CLASS (klass),
      G_SIGNAL_RUN_FIRST, 0, NULL,
      NULL, NULL, G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER);

  // Args: CdiConnectionStatus, CdiEndpointHandle
  connection_signals[STATUS_CHANGED] =
      g_signal_new ("status-changed", G_TYPE_FROM_CLASS (klass),
      G_SIGNAL_RUN_FIRST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_INT);
}

static void
gst_aws_cdi_connection_init (GstAwsCdiConnection * self)
{
  g_mutex_init (&self->lock);
  self->adapter_size = 0;
}

static GstAwsCdiConnection *
gst_aws_cdi_connection_new (const gchar * src_address,
    const gchar * dest_address, gint dest_port)
{
  GstAwsCdiConnection *ret = g_object_new (GST_TYPE_AWS_CDI_CONNECTION, NULL);

  ret->src_address = g_strdup (src_address);
  ret->dest_address = g_strdup (dest_address);
  ret->dest_port = dest_port;

  return ret;
}

static void
_connection_cb (const CdiCoreConnectionCbData * cb_data_ptr)
{
  GstAwsCdiConnection *conn =
      GST_AWS_CDI_CONNECTION (cb_data_ptr->connection_user_cb_param);

  g_signal_emit (conn, connection_signals[STATUS_CHANGED], 0,
      cb_data_ptr->status_code);
}

static void
TestAvmTxCallback (const CdiAvmTxCbData * cb_data_ptr)
{
  GstAwsCdiSink *sink =
      GST_AWS_CDI_SINK (cb_data_ptr->core_cb_data.user_cb_param);

  if (cb_data_ptr->core_cb_data.status_code == kCdiStatusOk) {
    GST_LOG_OBJECT (sink, "Sent buffer successfully");
  } else {
    GST_ERROR_OBJECT (sink, "Failed to send buffer: %s",
        cb_data_ptr->core_cb_data.err_msg_str);
    GST_ELEMENT_ERROR (sink, STREAM, FAILED,
        ("Failed to send buffer"), ("%s",
            cb_data_ptr->core_cb_data.err_msg_str));
  }

  GST_OBJECT_LOCK (sink);
  sink->rendered = TRUE;
  g_cond_broadcast (&sink->cond);
  GST_OBJECT_UNLOCK (sink);
}

static gboolean
gst_aws_cdi_connection_fulfill_endpoint (GstAwsCdiConnection * conn,
    guint64 adapter_size, guint64 * adapter_offset, guint16 * stream_id)
{
  gboolean ret = FALSE;
  gboolean emit_signal = FALSE;
  gpointer adapter_tx_buffer_ptr = NULL;

  g_mutex_lock (&conn->lock);

  if (conn->reservations == 0) {
    goto done;
  }

  *adapter_offset = conn->adapter_size;
  conn->adapter_size += adapter_size;

  conn->reservations -= 1;
  *stream_id = conn->endpoints;
  conn->endpoints += 1;

  if (conn->reservations == 0) {
    CdiReturnStatus rs;
    CdiAdapterData adapter_data = {
      .adapter_ip_addr_str = conn->src_address,
      .tx_buffer_size_bytes = conn->adapter_size,
      .ret_tx_buffer_ptr = NULL,
      .adapter_type = kCdiAdapterTypeEfa
    };
    rs = CdiCoreNetworkAdapterInitialize (&adapter_data, &conn->adapter_handle);

    if (rs != kCdiStatusOk) {
      GST_ERROR_OBJECT (conn,
          "Encountered error while initializing adapter: %s",
          CdiCoreStatusToString (rs));
      goto done;
    } else {
      CdiLogMethodData log_method_data = {
        .log_method = kLogMethodStdout
      };
      CdiTxConfigData config_data = {
        .adapter_handle = conn->adapter_handle,
        .dest_ip_addr_str = conn->dest_address,
        .dest_port = conn->dest_port,
        .shared_thread_id = 0,
        .thread_core_num = -1,
        .connection_name_str = NULL,
        .connection_log_method_data_ptr = &log_method_data,

        .connection_cb_ptr = _connection_cb,
        .connection_user_cb_param = conn,

        .stats_cb_ptr = NULL,
        .stats_user_cb_param = NULL,
        .stats_config.stats_period_seconds = 0,
        .stats_config.disable_cloudwatch_stats = true,
      };

      rs = CdiAvmTxCreate (&config_data, TestAvmTxCallback,
          &conn->connection_handle);

      if (rs != kCdiStatusOk) {
        GST_ERROR_OBJECT (conn,
            "Encountered error while creating connection: %s",
            CdiCoreStatusToString (rs));
        goto done;
      } else {
        adapter_tx_buffer_ptr = adapter_data.ret_tx_buffer_ptr;
        emit_signal = TRUE;
      }
    }
  }

  ret = TRUE;

done:
  g_mutex_unlock (&conn->lock);

  if (emit_signal) {
    g_signal_emit (conn, connection_signals[CREATED], 0,
        conn->connection_handle, adapter_tx_buffer_ptr);
  }

  return ret;
}

struct _GstAwsCdiConnections
{
  GObject parent;
  GHashTable *map;
  GMutex lock;
};

G_DEFINE_TYPE (GstAwsCdiConnections, gst_aws_cdi_connections, G_TYPE_OBJECT);

static void
gst_aws_cdi_connections_finalize (GObject * object)
{
  GstAwsCdiConnections *self = GST_AWS_CDI_CONNECTIONS (object);

  g_mutex_clear (&self->lock);
  g_hash_table_unref (self->map);

  G_OBJECT_CLASS (gst_aws_cdi_connections_parent_class)->finalize (object);
}

static GstAwsCdiConnections *
gst_aws_cdi_connections_new ()
{
  return g_object_new (GST_TYPE_AWS_CDI_CONNECTIONS, NULL);
}

static void
gst_aws_cdi_connections_release_endpoint (GstAwsCdiConnections * connections,
    GstAwsCdiConnection * connection)
{
  gchar *key = NULL;

  g_mutex_lock (&connections->lock);
  g_mutex_lock (&connection->lock);
  g_assert (connection->endpoints > 0);
  connection->endpoints -= 1;
  if (connection->endpoints == 0) {
    key = g_strdup_printf ("%s->%s:%d",
        connection->src_address,
        connection->dest_address, connection->dest_port);
  }
  g_mutex_unlock (&connection->lock);

  if (key) {
    g_hash_table_remove (connections->map, key);
    g_free (key);
  }

  g_mutex_unlock (&connections->lock);
}

static GstAwsCdiConnection *
gst_aws_cdi_connections_reserve_endpoint (GstAwsCdiConnections * connections,
    const gchar * src_address, const gchar * dest_address, gint dest_port)
{
  GstAwsCdiConnection *ret = NULL;
  gchar *key =
      g_strdup_printf ("%s->%s:%d", src_address, dest_address, dest_port);

  g_mutex_lock (&connections->lock);

  ret = (GstAwsCdiConnection *) g_hash_table_lookup (connections->map, key);

  if (ret) {
    g_mutex_lock (&ret->lock);
    if (ret->reservations == 0) {
      // connection is already running
      ret = NULL;
      g_mutex_unlock (&ret->lock);
      goto done;
    }
  } else {
    ret = gst_aws_cdi_connection_new (src_address, dest_address, dest_port);
    g_hash_table_insert (connections->map, key, ret);
    key = NULL;
    g_mutex_lock (&ret->lock);
  }

  ret->reservations += 1;

  g_mutex_unlock (&ret->lock);

done:
  g_mutex_unlock (&connections->lock);
  g_free (key);

  return ret;
}

#define GST_AWS_CDI_CONNECTIONS_CONTEXT "gst.aws.cdi.connections"

static void
gst_aws_cdi_connections_query (GstElement * element,
    GstAwsCdiConnections ** connections_ptr)
{
  GstQuery *query;
  GstAwsCdiConnections *connections = NULL;

  query = gst_query_new_context (GST_AWS_CDI_CONNECTIONS_CONTEXT);
  if (gst_pad_peer_query (GST_BASE_SINK_PAD (element), query)) {
    GstContext *context;

    gst_query_parse_context (query, &context);
    gst_element_set_context (GST_ELEMENT_CAST (element), context);
  } else {
    GstMessage *message;

    message =
        gst_message_new_need_context (GST_OBJECT_CAST (element),
        GST_AWS_CDI_CONNECTIONS_CONTEXT);
    gst_element_post_message (GST_ELEMENT_CAST (element), message);
  }
  gst_query_unref (query);

  GST_OBJECT_LOCK (element);
  if (!*connections_ptr) {
    connections = gst_aws_cdi_connections_new ();
  }
  GST_OBJECT_UNLOCK (element);

  if (connections) {
    GstContext *context;
    GstMessage *message;
    GstStructure *s;

    context = gst_context_new (GST_AWS_CDI_CONNECTIONS_CONTEXT, TRUE);
    s = gst_context_writable_structure (context);
    gst_structure_set (s, "connections", GST_TYPE_AWS_CDI_CONNECTIONS,
        connections, NULL);
    g_object_unref (connections);

    gst_element_set_context (GST_ELEMENT_CAST (element), context);
    message = gst_message_new_have_context (GST_OBJECT_CAST (element), context);
    gst_element_post_message (GST_ELEMENT_CAST (element), message);
  }
}

static void
gst_aws_cdi_connections_class_init (GstAwsCdiConnectionsClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->finalize = gst_aws_cdi_connections_finalize;
}

static void
gst_aws_cdi_connections_init (GstAwsCdiConnections * self)
{
  g_mutex_init (&self->lock);
  self->map =
      g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
}

#define DEFAULT_DEST_ADDRESS "127.0.0.1"
#define DEFAULT_DEST_PORT 2000
#define DEFAULT_SRC_ADDRESS "0.0.0.0"

#define GST_ELEMENT_PROGRESS(el, type, code, text)      \
  G_STMT_START {                                          \
    gchar *__txt = _gst_element_error_printf text;        \
    gst_element_post_message (GST_ELEMENT_CAST (el),      \
        gst_message_new_progress (GST_OBJECT_CAST (el),   \
          GST_PROGRESS_TYPE_ ##type, code, __txt));     \
    g_free (__txt);                                       \
  } G_STMT_END

static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-raw,"
        "width = (int) [ 1, MAX ],"
        "height = (int) [ 1, MAX ],"
        "framerate = (fraction) [ 0, MAX ],"
        "format = (string) RGB,"
        "colorimetry = (string) 1:1:1:1,"
        "pixel-aspect-ratio = (fraction) 1/1,"
        "interlace-mode = (string) progressive,;"
        /* SMPTE ST 2110-30:2017 Table 1 */
        /* M (Mono) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=1, rate={48000, 96000};"
        /* ST (Standard Stereo) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=2, rate={48000, 96000};"
        /* DM (Dual Mono) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=2, channel-mask=(bitmask)0x0, rate={48000, 96000};"
        /* GStreamer has no notion of LtRt (Matrix Stereo) */
        /* 51 (5.1 Surround) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=6, channel-mask=(bitmask)0x3f, rate={48000, 96000};"
        /* 71 (7.1 Surround) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=8, channel-mask=(bitmask)0xc3f, rate={48000, 96000};"
        /* 222 (22.2 Surround) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=24, channel-mask=(bitmask)0xffffff, rate={48000, 96000};"
        /* TODO: SGRP (SDI audio group): would need a simple muxer and dedicated caps */
    )
    );

enum
{
  PROP_0,
  PROP_DEST_ADDRESS,
  PROP_DEST_PORT,
  PROP_SRC_ADDRESS,
};

#define parent_class gst_aws_cdi_sink_parent_class
G_DEFINE_TYPE (GstAwsCdiSink, gst_aws_cdi_sink, GST_TYPE_BASE_SINK);
GST_ELEMENT_REGISTER_DEFINE (aws_cdi_sink, "awscdisink", GST_RANK_NONE,
    GST_TYPE_AWS_CDI_SINK);

static CdiReturnStatus
avm_config_from_vinfo (CdiAvmConfig * avm_config_ptr,
    int *payload_unit_size_ptr, GstVideoInfo * vinfo)
{
  CdiAvmBaselineConfig baseline_config = {
    .payload_type = kCdiAvmVideo,
    .video_config = {
          .version.major = 02,
          .version.minor = 00,
          .width = (uint16_t) vinfo->width,
          .height = (uint16_t) vinfo->height,
          .sampling = kCdiAvmVidRGB,
          .alpha_channel = kCdiAvmAlphaUnused,
          .depth = kCdiAvmVidBitDepth8,
          .frame_rate_num = (uint32_t) vinfo->fps_n,
          .frame_rate_den = (uint32_t) vinfo->fps_d,
          .colorimetry = kCdiAvmVidColorimetryBT709,
          .interlace = false,
          .segmented = false,
          .tcs = kCdiAvmVidTcsSDR,
          .range = kCdiAvmVidRangeFull,
          .par_width = 1,
          .par_height = 1,
          .start_vertical_pos = 0,
          .vertical_size = 0,
          .start_horizontal_pos = 0,
        .horizontal_size = 0}
  };
  return CdiAvmMakeBaselineConfiguration (&baseline_config, avm_config_ptr,
      payload_unit_size_ptr);
}

static CdiReturnStatus
avm_config_from_ainfo (CdiAvmConfig * avm_config_ptr,
    int *payload_unit_size_ptr, GstAudioInfo * ainfo,
    const gchar * language_code)
{
  CdiAvmAudioSampleRate sample_rate_khz;
  CdiAvmAudioChannelGrouping grouping;

  switch (ainfo->rate) {
    case 48000:
      sample_rate_khz = kCdiAvmAudioSampleRate48kHz;
      break;
    case 96000:
      sample_rate_khz = kCdiAvmAudioSampleRate96kHz;
      break;
    default:
      g_assert_not_reached ();
      break;
  }

  if (ainfo->channels == 1)
    grouping = kCdiAvmAudioM;
  else if (ainfo->channels == 2 && GST_AUDIO_INFO_IS_UNPOSITIONED (ainfo))
    grouping = kCdiAvmAudioDM;
  else if (ainfo->channels == 2)
    grouping = kCdiAvmAudioST;
  else if (ainfo->channels == 6)
    grouping = kCdiAvmAudio51;
  else if (ainfo->channels == 8)
    grouping = kCdiAvmAudio71;
  else if (ainfo->channels == 24)
    grouping = kCdiAvmAudio222;
  else
    g_assert_not_reached ();

  CdiAvmBaselineConfig baseline_config = {
    .payload_type = kCdiAvmAudio,
    .audio_config = {
          .version.major = 02,
          .version.minor = 00,
          .grouping = grouping,
          .sample_rate_khz = sample_rate_khz,
          .language = "\0\0\0",
        }
  };

  if (language_code) {
    strncpy (baseline_config.audio_config.language, language_code, MIN (3,
            strlen (language_code)));
  }

  return CdiAvmMakeBaselineConfiguration (&baseline_config, avm_config_ptr,
      payload_unit_size_ptr);
}

static CdiReturnStatus
make_meta_config (CdiAvmConfig * avm_config_ptr, int *payload_unit_size_ptr)
{
  CdiAvmBaselineConfig baseline_config = {
    .payload_type = kCdiAvmAncillary,
    .ancillary_data_config = {
          .version.major = 02,
          .version.minor = 00,
        }
  };

  return CdiAvmMakeBaselineConfiguration (&baseline_config, avm_config_ptr,
      payload_unit_size_ptr);
}

static void
gst_aws_cdi_sink_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstAwsCdiSink *sink = GST_AWS_CDI_SINK (object);

  switch (property_id) {
    case PROP_DEST_PORT:
      GST_OBJECT_LOCK (sink);
      sink->dest_port = g_value_get_int (value);
      GST_OBJECT_UNLOCK (sink);
      break;
    case PROP_DEST_ADDRESS:
      GST_OBJECT_LOCK (sink);
      sink->dest_address = g_value_dup_string (value);
      GST_OBJECT_UNLOCK (sink);
      break;
    case PROP_SRC_ADDRESS:
      GST_OBJECT_LOCK (sink);
      sink->src_address = g_value_dup_string (value);
      GST_OBJECT_UNLOCK (sink);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
gst_aws_cdi_sink_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstAwsCdiSink *sink = GST_AWS_CDI_SINK (object);

  switch (property_id) {
    case PROP_DEST_PORT:
      GST_OBJECT_LOCK (sink);
      g_value_set_int (value, sink->dest_port);
      GST_OBJECT_UNLOCK (sink);
      break;
    case PROP_DEST_ADDRESS:
      GST_OBJECT_LOCK (sink);
      g_value_set_string (value, sink->dest_address);
      GST_OBJECT_UNLOCK (sink);
      break;
    case PROP_SRC_ADDRESS:
      GST_OBJECT_LOCK (sink);
      g_value_set_string (value, sink->src_address);
      GST_OBJECT_UNLOCK (sink);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
gst_aws_cdi_sink_finalize (GObject * object)
{
  GstAwsCdiSink *sink = GST_AWS_CDI_SINK (object);

  g_cond_clear (&sink->cond);
  g_free (sink->dest_address);
  g_free (sink->src_address);
  g_free (sink->language_code);
  g_clear_object (&sink->connections);
  gst_caps_replace (&sink->caps, NULL);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static CdiReturnStatus
update_av_config_unlocked (GstAwsCdiSink * sink)
{
  CdiReturnStatus ret = kCdiStatusFatal;
  int payload_unit_size;

  if (sink->caps == NULL || !gst_caps_is_fixed (sink->caps)) {
    ret = kCdiStatusOk;
    goto done;
  }

  memset (&sink->av_config, 0, sizeof (sink->av_config));

  if (gst_structure_has_name (gst_caps_get_structure (sink->caps, 0),
          "video/x-raw")) {
    GstVideoInfo vinfo;

    if (!gst_video_info_from_caps (&vinfo, sink->caps)) {
      GST_ERROR_OBJECT (sink, "Failed to parse video caps");
      goto done;
    }

    ret = avm_config_from_vinfo (&sink->av_config, &payload_unit_size, &vinfo);

    sink->adapter_size = vinfo.size;
  } else if (gst_structure_has_name (gst_caps_get_structure (sink->caps, 0),
          "audio/x-raw")) {
    GstAudioInfo ainfo;

    if (!gst_audio_info_from_caps (&ainfo, sink->caps)) {
      GST_ERROR_OBJECT (sink, "Failed to parse audio caps");
      goto done;
    }

    ret =
        avm_config_from_ainfo (&sink->av_config, &payload_unit_size, &ainfo,
        sink->language_code);

    sink->adapter_size =
        GST_CLOCK_TIME_TO_FRAMES (50 * GST_MSECOND,
        ainfo.rate) * GST_AUDIO_INFO_BPF (&ainfo);
  } else {
    g_assert_not_reached ();
  }

  sink->send_av_config = TRUE;

done:
  return ret;
}

static gboolean
gst_aws_cdi_sink_event (GstBaseSink * bsink, GstEvent * event)
{
  GstAwsCdiSink *sink = GST_AWS_CDI_SINK (bsink);
  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_TAG:{
      GstTagList *l;

      gst_event_parse_tag (event, &l);

      g_free (sink->language_code);
      sink->language_code = NULL;

      gst_tag_list_get_string (l, GST_TAG_LANGUAGE_CODE, &sink->language_code);

      GST_OBJECT_LOCK (sink);
      update_av_config_unlocked (sink);
      GST_OBJECT_UNLOCK (sink);

      break;
    }
    default:
      break;
  }

  return GST_BASE_SINK_CLASS (parent_class)->event (bsink, event);
}

static gboolean
gst_aws_cdi_sink_set_caps (GstBaseSink * bsink, GstCaps * caps)
{
  GstAwsCdiSink *sink = GST_AWS_CDI_SINK (bsink);
  gboolean ret = FALSE;
  CdiReturnStatus rs;

  if (sink->caps != NULL) {
    GST_ERROR_OBJECT (sink, "connection renegotiation unsupported");
    goto done;
  }

  GST_INFO_OBJECT (sink, "setting caps: %" GST_PTR_FORMAT, caps);

  GST_OBJECT_LOCK (sink);

  sink->connection_status = kCdiConnectionStatusDisconnected;

  gst_caps_replace (&sink->caps, caps);

  if ((rs = update_av_config_unlocked (sink)) != kCdiStatusOk) {
    GST_ERROR_OBJECT (sink, "Failed to build AVMConfig from gst info: %s",
        CdiCoreStatusToString (rs));
    GST_OBJECT_UNLOCK (sink);
    goto done;
  }

  GST_OBJECT_UNLOCK (sink);

  GST_DEBUG_OBJECT (sink, "Requesting adapter size: %lu", sink->adapter_size);

  if (!gst_aws_cdi_connection_fulfill_endpoint (sink->connection,
          sink->adapter_size, &sink->adapter_offset, &sink->stream_id)) {
    GST_ERROR_OBJECT (sink, "Failed to fulfill endpoint");
    goto done;
  }

  sink->send_av_config = TRUE;

  ret = TRUE;

done:
  return ret;
}

typedef struct
{
  /* The buffer we are iterating SCTE 104 meta on */
  GstBuffer *buffer;
  /* The opaque iterating state */
  gpointer meta_state;
  GstAwsCdiSink *sink;
} AncillaryDataCtx;

const CdiAvmAncillaryDataPacket *
PacketizeAncCb (void *user_data)
{
  AncillaryDataCtx *ctx = (AncillaryDataCtx *) user_data;
  CdiAvmAncillaryDataPacket *ret = NULL;
  GstSCTE104Packet scte_104_pkt = { 0, };
  uint8_t *buf = NULL;
  GstSCTE104Meta *scte_104_meta;
  CdiAvmAncillaryDataPacket *packet;

  gst_scte_104_packet_init (&scte_104_pkt, 0xffff);

  if ((scte_104_meta =
          (GstSCTE104Meta *) gst_buffer_iterate_meta_filtered (ctx->buffer,
              &ctx->meta_state, GST_SCTE_104_META_API_TYPE)) == NULL) {
    goto done;
  }

  if (scte_104_meta->size > 254) {
    GST_ERROR ("SCTE 104 message fragmentation not supported");
    goto done;
  }

  packet = g_new0 (CdiAvmAncillaryDataPacket, 1);

  packet->is_color_difference_channel = false;
  /*  Without specific line location within the field or frame */
  packet->line_number = 0x7ff;
  /* Without specific horizontal location  */
  packet->horizontal_offset = 0xFFF;
  packet->is_valid_source_stream_number = false;
  packet->source_stream_number = 0;
  packet->did = 0x41;
  packet->sdid = 0x07;

  // + 1 for descriptor byte
  packet->data_count = scte_104_meta->size + 1;

  // Payload descriptor byte, no fragmentation
  packet->user_data[0] = 0x8;
  memcpy (packet->user_data + 1, (void *) scte_104_meta->data,
      scte_104_meta->size);

  ret = packet;

  GST_TRACE_OBJECT (ctx->sink, "Packetized one SCTE 104 packet");

done:
  if (buf)
    free (buf);

  gst_scte_104_packet_unset (&scte_104_pkt);

  return ret;
}

static GstFlowReturn
render_metadata (GstAwsCdiSink * sink, GstBuffer * buffer)
{
  AncillaryDataCtx ctx = { 0, };
  CdiReturnStatus rs;
  int size_in_bytes = 0;
  CdiFieldKind field_kind = kCdiFieldKindUnspecified;
  GstFlowReturn ret = GST_FLOW_OK;

  ctx.buffer = buffer;
  ctx.sink = sink;

  size_in_bytes = sink->adapter_size;
  /* We use this sink's region of the adapter to send ancillary data over,
   * we can assume that ancillary data size for one frame will never
   * exceed the size of the frame itself. If not, this call will simply error
   * out anyway
   */
  rs = CdiAvmPacketizeAncillaryData (PacketizeAncCb, field_kind, &ctx,
      sink->adapter_tx_buffer_ptr, &size_in_bytes);

  if (rs == kCdiStatusOk && size_in_bytes) {
    CdiSglEntry sgl_entry = {
      .address_ptr = sink->adapter_tx_buffer_ptr,
      .size_in_bytes = (int) size_in_bytes,
    };
    CdiSgList sgl = {
      .total_data_size = (int) size_in_bytes,
      .sgl_head_ptr = &sgl_entry,
      .sgl_tail_ptr = &sgl_entry,
      .internal_data_ptr = NULL,
    };
    CdiAvmTxPayloadConfig payload_config = {
      .core_config_data = {
            .core_extra_data = {
                  .origination_ptp_timestamp = {0,},
                  .payload_user_data = 0,
                },
            .user_cb_param = sink,
            .unit_size = 24,
          },
      .avm_extra_data = {
            .stream_identifier = 42,
          }
    };
    sink->rendered = FALSE;

    GST_LOG_OBJECT (sink, "Packetized %d bytes of ancillary data",
        size_in_bytes);

    if (sink->send_meta_config) {
      CdiAvmConfig meta_config;
      int payload_unit_size;

      make_meta_config (&meta_config, &payload_unit_size);
      rs = CdiAvmTxPayload (sink->connection->connection_handle,
          &payload_config, &meta_config, &sgl, 0);
      sink->send_meta_config = FALSE;
    } else {
      rs = CdiAvmTxPayload (sink->connection->connection_handle,
          &payload_config, NULL, &sgl, 0);
    }
  }

  if (rs == kCdiStatusOk) {
    while (!sink->rendered) {
      g_cond_wait (&sink->cond, GST_OBJECT_GET_LOCK (sink));
      if (sink->unlock) {
        GST_OBJECT_UNLOCK (sink);
        ret = gst_base_sink_wait_preroll (GST_BASE_SINK (sink));
        if (ret == GST_FLOW_OK)
          GST_OBJECT_LOCK (sink);
        else
          goto done;
      }
    }
  } else {
    GST_ERROR_OBJECT (sink, "Failed to send metadata payload");
    ret = GST_FLOW_ERROR;
  }

done:
  return ret;
}


static GstFlowReturn
gst_aws_cdi_sink_render (GstBaseSink * bsink, GstBuffer * buffer)
{
  GstAwsCdiSink *sink = GST_AWS_CDI_SINK (bsink);
  GstFlowReturn ret;
  GstMapInfo map;
  gsize size;
  CdiReturnStatus rs = kCdiStatusOk;

  GST_OBJECT_LOCK (sink);

  if (sink->unlock) {
    GST_OBJECT_UNLOCK (sink);
    ret = GST_FLOW_FLUSHING;
    goto done;
  }

  while (sink->connection_status != kCdiConnectionStatusConnected) {
    g_cond_wait (&sink->cond, GST_OBJECT_GET_LOCK (sink));
    if (sink->unlock) {
      GST_OBJECT_UNLOCK (sink);
      ret = gst_base_sink_wait_preroll (bsink);
      if (ret == GST_FLOW_OK)
        GST_OBJECT_LOCK (sink);
      else
        goto done;
    }
  }

  gst_buffer_map (buffer, &map, GST_MAP_READ);
  size = map.size;

  if (size > sink->adapter_size) {
    GST_ERROR_OBJECT (sink,
        "cannot process buffers with size %" G_GSIZE_FORMAT
        " larger than reserved adapter size %" G_GSIZE_FORMAT
        ", audio buffer duration cannot exceed 50 milliseconds", size,
        sink->adapter_size);
    GST_OBJECT_UNLOCK (sink);
    gst_buffer_unmap (buffer, &map);
    ret = GST_FLOW_ERROR;
    goto done;
  }

  if ((ret = render_metadata (sink, buffer)) != GST_FLOW_OK) {
    GST_OBJECT_UNLOCK (sink);
    gst_buffer_unmap (buffer, &map);
    goto done;
  }

  memcpy (sink->adapter_tx_buffer_ptr, map.data, size);
  gst_buffer_unmap (buffer, &map);

  if (rs == kCdiStatusOk) {
    CdiSglEntry sgl_entry = {
      .address_ptr = sink->adapter_tx_buffer_ptr,
      .size_in_bytes = (int) size,
    };
    CdiSgList sgl = {
      .total_data_size = (int) size,
      .sgl_head_ptr = &sgl_entry,
      .sgl_tail_ptr = &sgl_entry,
      .internal_data_ptr = NULL,
    };
    CdiAvmTxPayloadConfig payload_config = {
      .core_config_data = {
            .core_extra_data = {
                  .origination_ptp_timestamp = {0,},
                  .payload_user_data = 0,
                },
            .user_cb_param = sink,
            .unit_size = 24,
          },
      .avm_extra_data = {
            .stream_identifier = sink->stream_id,
          }
    };
    sink->rendered = FALSE;
    rs = CdiAvmTxPayload (sink->connection->connection_handle, &payload_config,
        sink->send_av_config ? &sink->av_config : NULL, &sgl, 0);
    sink->send_av_config = FALSE;

    while (!sink->rendered) {
      g_cond_wait (&sink->cond, GST_OBJECT_GET_LOCK (sink));
      if (sink->unlock) {
        GST_OBJECT_UNLOCK (sink);
        ret = gst_base_sink_wait_preroll (bsink);
        if (ret == GST_FLOW_OK)
          GST_OBJECT_LOCK (sink);
        else
          goto done;
      }
    }
  }

  GST_OBJECT_UNLOCK (sink);

  ret = rs == kCdiStatusOk ? GST_FLOW_OK : GST_FLOW_ERROR;

done:
  return ret;
}

static void
_connection_created_cb (GstAwsCdiConnection * connection,
    CdiConnectionHandle connection_handle,
    gpointer adapter_tx_buffer_ptr, GstAwsCdiSink * sink)
{
  sink->adapter_tx_buffer_ptr = adapter_tx_buffer_ptr + sink->adapter_offset;
}

static void
_connection_status_changed_cb (GstAwsCdiConnection * connection,
    CdiConnectionStatus status_code, GstAwsCdiSink * sink)
{
  GST_INFO_OBJECT (sink, "Connection status changed: %d", status_code);
  if (status_code == kCdiConnectionStatusConnected) {
    GST_OBJECT_LOCK (sink);
    sink->connection_status = status_code;
    g_cond_broadcast (&sink->cond);
    GST_OBJECT_UNLOCK (sink);

    GST_ELEMENT_PROGRESS (sink, COMPLETE, "connect", ("Connected receiver"));
  } else {
    GST_ERROR_OBJECT (sink, "Disconnected");
    GST_ELEMENT_ERROR (sink, RESOURCE, OPEN_READ_WRITE,
        ("Disconnected."), (NULL));
  }
}

static gboolean
gst_aws_cdi_sink_open (GstBaseSink * bsink)
{
  GstAwsCdiSink *sink = GST_AWS_CDI_SINK (bsink);
  gboolean ret = FALSE;
  CdiReturnStatus rs;

  rs = gst_aws_ensure_cdi_initialized ();

  if (kCdiStatusOk != rs) {
    GST_ERROR_OBJECT (bsink,
        "SDK core initialize failed. Error=[%d], Message=[%s]", rs,
        CdiCoreStatusToString (rs));
    goto done;
  }

  sink->connections = NULL;
  sink->send_meta_config = TRUE;
  gst_aws_cdi_connections_query (GST_ELEMENT (bsink), &sink->connections);
  g_assert (sink->connections != NULL);

  if (!(sink->connection =
          gst_aws_cdi_connections_reserve_endpoint (sink->connections,
              sink->src_address, sink->dest_address, sink->dest_port))) {
    GST_ERROR_OBJECT (bsink, "Failed to reserve endpoint");
    goto done;
  }

  g_signal_connect (sink->connection, "created",
      G_CALLBACK (_connection_created_cb), sink);

  g_signal_connect (sink->connection, "status-changed",
      G_CALLBACK (_connection_status_changed_cb), sink);

  GST_ELEMENT_PROGRESS (sink, START, "connect", ("Connecting receiver"));

  GST_INFO_OBJECT (sink, "Connection reserved: %p", sink->connection);

  ret = TRUE;

done:
  return ret;
}

static gboolean
gst_aws_cdi_sink_close (GstBaseSink * bsink)
{
  GstAwsCdiSink *sink = GST_AWS_CDI_SINK (bsink);

  if (sink->connections && sink->connection) {
    gst_aws_cdi_connections_release_endpoint (sink->connections,
        sink->connection);
    sink->connection = NULL;
  }

  g_clear_object (&sink->connections);

  g_free (sink->language_code);
  sink->language_code = NULL;

  gst_caps_replace (&sink->caps, NULL);

  return TRUE;
}

static gboolean
gst_aws_cdi_sink_unlock (GstBaseSink * bsink)
{
  GstAwsCdiSink *self = GST_AWS_CDI_SINK (bsink);

  GST_OBJECT_LOCK (self);
  self->unlock = TRUE;
  GST_OBJECT_UNLOCK (self);

  g_cond_broadcast (&self->cond);
  return TRUE;
}

static gboolean
gst_aws_cdi_sink_unlock_stop (GstBaseSink * bsink)
{
  GstAwsCdiSink *self = GST_AWS_CDI_SINK (bsink);

  GST_OBJECT_LOCK (self);
  self->unlock = FALSE;
  GST_OBJECT_UNLOCK (self);

  return TRUE;
}

static void
gst_aws_cdi_sink_set_context (GstElement * element, GstContext * context)
{
  GstAwsCdiSink *sink = GST_AWS_CDI_SINK (element);

  if (g_strcmp0 (gst_context_get_context_type (context),
          GST_AWS_CDI_CONNECTIONS_CONTEXT) == 0) {
    const GstStructure *s = gst_context_get_structure (context);

    GST_OBJECT_LOCK (sink);

    g_clear_object (&sink->connections);
    gst_structure_get (s, "connections", GST_TYPE_AWS_CDI_CONNECTIONS,
        &sink->connections, NULL);

    GST_DEBUG_OBJECT (sink, "Setting connections table %p", sink->connections);
    GST_OBJECT_UNLOCK (sink);
  }

  GST_ELEMENT_CLASS (parent_class)->set_context (element, context);
}

static void
gst_aws_cdi_sink_class_init (GstAwsCdiSinkClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GstBaseSinkClass *basesink_class = GST_BASE_SINK_CLASS (klass);

  gobject_class->set_property = gst_aws_cdi_sink_set_property;
  gobject_class->get_property = gst_aws_cdi_sink_get_property;
  gobject_class->finalize = gst_aws_cdi_sink_finalize;

  basesink_class->set_caps = GST_DEBUG_FUNCPTR (gst_aws_cdi_sink_set_caps);
  basesink_class->render = GST_DEBUG_FUNCPTR (gst_aws_cdi_sink_render);
  basesink_class->start = GST_DEBUG_FUNCPTR (gst_aws_cdi_sink_open);
  basesink_class->stop = GST_DEBUG_FUNCPTR (gst_aws_cdi_sink_close);
  basesink_class->unlock = GST_DEBUG_FUNCPTR (gst_aws_cdi_sink_unlock);
  basesink_class->unlock_stop =
      GST_DEBUG_FUNCPTR (gst_aws_cdi_sink_unlock_stop);
  basesink_class->event = GST_DEBUG_FUNCPTR (gst_aws_cdi_sink_event);

  element_class->set_context = GST_DEBUG_FUNCPTR (gst_aws_cdi_sink_set_context);

  g_object_class_install_property (gobject_class, PROP_DEST_ADDRESS,
      g_param_spec_string ("dest-address", "Destination address",
          "The address of the receiver",
          DEFAULT_DEST_ADDRESS,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
              GST_PARAM_MUTABLE_READY | G_PARAM_CONSTRUCT)));

  g_object_class_install_property (gobject_class, PROP_DEST_PORT,
      g_param_spec_int ("dest-port", "Destination port",
          "The port the receiver will receive on", 1, G_MAXUINT16,
          DEFAULT_DEST_PORT,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
              GST_PARAM_MUTABLE_READY | G_PARAM_CONSTRUCT)));

  g_object_class_install_property (gobject_class, PROP_SRC_ADDRESS,
      g_param_spec_string ("src-address", "Source address",
          "The address to send from",
          DEFAULT_SRC_ADDRESS,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
              GST_PARAM_MUTABLE_READY | G_PARAM_CONSTRUCT)));

  gst_element_class_add_static_pad_template (element_class, &sink_template);

  gst_element_class_set_static_metadata (element_class, "AWS CDI Sink",
      "Audio/Video/Sink", "AWS CDI Sink",
      "Mathieu Duponchelle <mathieu@centricular.com>");

  GST_DEBUG_CATEGORY_INIT (gst_aws_cdi_sink_debug, "awscdisink",
      0, "debug category for awscdisink element");
}

static void
gst_aws_cdi_sink_init (GstAwsCdiSink * self)
{
  self->unlock = FALSE;
  g_cond_init (&self->cond);

}
