/*
 * Copyright (c) 2016 Kernel Labs Inc. All Rights Reserved
 *
 * Address: Kernel Labs Inc., PO Box 745, St James, NY. 11780
 * Contact: sales@kernellabs.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @file	vanc-scte_104.h
 * @author	Steven Toth <stoth@kernellabs.com>
 * @copyright	Copyright (c) 2016 Kernel Labs Inc. All Rights Reserved.
 * @brief	SCTE-104 Automation System to Compression System Communications Applications Program Interface
 */

/**
 * Renamed to gst-scte-104.h and reworked by Mathieu Duponchelle
 * <mathieu@centricular.com> in order to integrate better within GStreamer.
 */

#ifndef _VANC_SCTE_104_H
#define _VANC_SCTE_104_H

#include <glib.h>
#include <gst/gst.h>

G_BEGIN_DECLS

#define SO_INIT_REQUEST_DATA     0x001

#define MO_SPLICE_REQUEST_DATA     0x101
#define MO_SPLICE_NULL_REQUEST_DATA  0x102
#define MO_TIME_SIGNAL_REQUEST_DATA  0x104
#define MO_INSERT_DESCRIPTOR_REQUEST_DATA    0x108
#define MO_INSERT_DTMF_REQUEST_DATA  0x109
#define MO_INSERT_AVAIL_DESCRIPTOR_REQUEST_DATA  0x10a
#define MO_INSERT_SEGMENTATION_REQUEST_DATA  0x10b
#define MO_PROPRIETARY_COMMAND_REQUEST_DATA  0x10c
#define MO_INSERT_TIER_DATA  0x10f
#define MO_INSERT_TIME_DESCRIPTOR  0x110

#define SPLICESTART_NORMAL    0x01
#define SPLICESTART_IMMEDIATE 0x02
#define SPLICEEND_NORMAL      0x03
#define SPLICEEND_IMMEDIATE   0x04
#define SPLICE_CANCEL         0x05

#define GST_SCTE_104_PACKET_MAX_PAYLOAD_LENGTH 2000

typedef struct
{
	/* single_operation_message */
	/* SCTE104 Spec 2012 - table 7.1 */
	guint16 op_id;
	guint16 message_size;
	guint16 result;
	guint16 result_extension;
	guint8 protocol_version;
	guint8 as_index;
	guint8 message_number;
	guint16 dpi_pid_index;
} GstSCTE104SingleOperation;

typedef struct
{
	/* SCTE Spec table 11.2 */
	guint8 time_type;
	union {
		struct {
			guint32 utc_seconds;
			guint16 utc_microseconds;
		} time_type_1;
		struct {
			guint8 hours;
			guint8 minutes;
			guint8 seconds;
			guint8 frames;
		} time_type_2;
		struct {
			guint8 gpi_number;
			guint8 gpi_edge;
		} time_type_3;
	};
} GstSCTE104MultipleOperationTimestamp;

typedef struct
{
	/* SCTE 104 Table 8-5 */
	guint32 splice_insert_type;
	guint32 splice_event_id;
	guint16 unique_program_id;
	guint16 pre_roll_time;	/* In 1/10's of a second */
	guint16 brk_duration;	/* In 1/10's of a second */
	guint8 avail_num;
	guint8 avails_expected;
	guint8 auto_return_flag;
} GstSCTE104SpliceRequestData;

typedef struct
{
	/* SCTE 104 Table 8-23 */
	guint16 pre_roll_time;	/* In milliseconds */
} GstSCTE104TimeSignalRequestData;

typedef struct
{
	/* SCTE 104 Table 8-26 */
	guint32 num_provider_avails;
	guint32 provider_avail_id[255];
} GstSCTE104AvailDescriptorRequestData;

typedef struct
{
	/* SCTE 104 Table 8-27 */
	guint32 descriptor_count;
	guint32 total_length;
	guint8 descriptor_bytes[255];
} GstSCTE104InsertDescriptorRequestData;

typedef struct
{
	/* SCTE 104 Table 8-28 */
	guint8 pre_roll_time;	/* In 1/10's of a second */
	guint32 dtmf_length;
	char dtmf_char[7];
} GstSCTE104DTMFDescriptorRequestData;

typedef struct
{
	/* SCTE 104 Table 8-29 */
	guint32 event_id;
	guint32 event_cancel_indicator;
	guint32 duration; /* In seconds */
	guint32 upid_type;
	guint32 upid_length;
	guint8 upid[255];
	guint32 type_id;
	guint32 segment_num;
	guint32 segments_expected;
	guint32 duration_extension_frames;
	guint32 delivery_not_restricted_flag;
	guint32 web_delivery_allowed_flag;
	guint32 no_regional_blackout_flag;
	guint32 archive_allowed_flag;
	guint32 device_restrictions;
} GstSCTE104SegmentationDescriptorRequestData;

typedef struct
{
	/* SCTE 104 2015 Table 9-32 */
	guint64 tai_seconds;
	guint32 tai_ns;
	guint32 utc_offset;
} GstSCTE104TimeDescriptorData;

typedef struct
{
	/* SCTE 104 Table 9-30 */
	guint32 proprietary_id;
	guint32 proprietary_command;
	guint32 data_length;
	guint8 proprietary_data[255];
} GstSCTE104ProprietaryCommandRequestData;

typedef struct
{
	/* SCTE 104 Table 9-31 */
	guint16 tier_data;
} GstSCTE104TierData;

typedef struct {
	guint16 op_id;
	guint16 data_length;
	guint8 *data;
	union {
		GstSCTE104SpliceRequestData sr_data;
		GstSCTE104TimeSignalRequestData timesignal_data;
		GstSCTE104DTMFDescriptorRequestData dtmf_data;
		GstSCTE104SegmentationDescriptorRequestData segmentation_data;
		GstSCTE104AvailDescriptorRequestData avail_descriptor_data;
		GstSCTE104InsertDescriptorRequestData descriptor_data;
		GstSCTE104ProprietaryCommandRequestData proprietary_data;
		GstSCTE104TierData tier_data;
		GstSCTE104TimeDescriptorData time_data;
	};
} GstSCTE104MultipleOperationOperation;

typedef struct
{
	/* multiple_operation_message */
	/* SCTE Spec table 7.2 */
	guint16 rsvd;
	guint16 message_size;
	guint8 protocol_version;
	guint8 as_index;
	guint8 message_number;
	guint16 dpi_pid_index;
	guint8 scte35_protocol_version;
	GstSCTE104MultipleOperationTimestamp timestamp;
	guint8 num_ops;
	GstSCTE104MultipleOperationOperation *ops;
} GstSCTE104MultipleOperation;

typedef struct
{
	/* See SMPTE 2010:2008 page 5 */
	guint8 payload_descriptor_byte;
	guint8 version;
	guint8 continued_pkt;
	guint8 following_pkt;
	guint8 duplicate_msg;

	GstSCTE104SingleOperation so_msg;
	GstSCTE104MultipleOperation mo_msg;
} GstSCTE104Packet;

typedef struct _GstSCTE104PacketParser GstSCTE104PacketParser;

void                    gst_scte_104_packet_init                             (GstSCTE104Packet *pkt,
                                                                              guint16 op_id);

void                    gst_scte_104_packet_unset                            (GstSCTE104Packet *pkt);

gboolean                gst_scte_104_packet_to_bytes                         (const GstSCTE104Packet *pkt,
                                                                              guint8 **bytes,
                                                                              guint16 *byte_count);

gboolean                gst_scte_104_packet_add_multiple_operation_operation (GstSCTE104Packet *pkt,
                                                                              guint16 op_id,
                                                                              GstSCTE104MultipleOperationOperation **op);

GstSCTE104PacketParser *gst_scte_104_packet_parser_new                       (void);

void                    gst_scte_104_packet_parser_free                      (GstSCTE104PacketParser *parser);

gboolean                gst_scte_104_packet_parser_parse                     (GstSCTE104PacketParser *parser,
                                                                              GstSCTE104Packet *pkt,
                                                                              const guint8 *payload,
                                                                              guint16 payload_length);

typedef struct {
  GstMeta meta;

  guint8 *data;
  gsize size;
} GstSCTE104Meta;

GType gst_scte_104_meta_api_get_type (void);
#define GST_SCTE_104_META_API_TYPE (gst_scte_104_meta_api_get_type())

const GstMetaInfo *gst_scte_104_meta_get_info (void);
#define GST_SCTE_104_META_INFO (gst_scte_104_meta_get_info())

#define gst_buffer_get_scte_104_meta(b) \
        ((GstSCTE104Meta*)gst_buffer_get_meta((b),GST_SCTE_104_META_API_TYPE))

GstSCTE104Meta *gst_buffer_add_scte_104_meta    (GstBuffer   * buffer,
							   guint8 *data, gsize size);

G_END_DECLS

#endif /* _VANC_SCTE_104_H */
