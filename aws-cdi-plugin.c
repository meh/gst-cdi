// GStreamer AWS CDI plugin
//
// Copyright (C) 2022 Tim-Philipp Müller <tim centricular com>
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include "aws-cdi-sink.h"
#include "aws-cdi-src.h"

static gboolean
plugin_init (GstPlugin * plugin)
{
  gboolean ret = TRUE;

  ret |= GST_ELEMENT_REGISTER (aws_cdi_sink, plugin);
  ret |= GST_ELEMENT_REGISTER (aws_cdi_src, plugin);

  return ret;
}

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    awscdi,
    "AWS CDI plugin",
    plugin_init, VERSION, "MPL-2.0", PACKAGE_NAME, GST_PACKAGE_ORIGIN)
