# GStreamer elements for the AWS CDI SDK

## Build

* Follow GStreamer's instructions for building the latest main branch and
  entering a devenv: <https://gitlab.freedesktop.org/gstreamer/gstreamer/-/blob/main/README.md>

* Cherry-pick the patch at https://github.com/aws/aws-cdi-sdk/pull/97

* Follow the installation guide for the AWS CDI SDK at
  <https://github.com/aws/aws-cdi-sdk/blob/mainline/INSTALL_GUIDE_LINUX.md>

* Export `PKG_CONFIG_PATH` to point to the SDK's pkg-config file, for example:

```
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/home/ec2-user/devel/cdi/aws-cdi-sdk/build/debug/lib64/pkgconfig/
```

* Export `LD_LIBRARY_PATH` to point to the directory where libcdisdk was build, for example:

```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ec2-user/devel/cdi/aws-cdi-sdk/build/debug/lib/
```

* Build the elements, setting the `cdi-sdk-libdir` and `/home/ec2-user/devel/cdi/aws-cdi-sdk/include/` options,
  for example:

```
meson build -Dcdi-sdk-libdir=/home/ec2-user/devel/cdi/aws-cdi-sdk/build/debug/lib/ -Dcdi-sdk-includedir=/home/ec2-user/devel/cdi/aws-cdi-sdk/include/
ninja -C build
```

Export `GST_PLUGIN_PATH` to point to the build directory, for example:

```
export GST_PLUGIN_PATH=$GST_PLUGIN_PATH:/home/ec2-user/devel/gst-cdi/build
```

At this point you should be able to inspect the plugin:

```
[main] [ec2-user@ip-172-31-38-224 gst-cdi]$ gst-inspect-1.0 awscdi
Plugin Details:
  Name                     awscdi
  Description              AWS CDI plugin
  Filename                 /home/ec2-user/devel/gst-cdi/build/libgstawscdi.so
  Version                  0.1
  License                  MPL-2.0
  Source module            gst-aws-cdi
  Binary package           gst-aws-cdi
  Origin URL               https://gitlab.freedesktop.org/meh/gst-cdi

  awscdisink: AWS CDI Sink
  awscdisrc: AWS CDI Source

  2 features:
  +-- 2 elements

```


And run pipelines such as (in two terminals):

```
gst-launch-1.0 audiotestsrc ! audio/x-raw, channels=24 ! taginject tags="language-code=\"de\"" ! awscdisink videotestsrc ! awscdisink
```

```
gst-launch-1.0 -e multiqueue name=mq mp4mux name=mux ! queue ! filesink location=muxed.mp4 awscdisrc name=src src. ! video/x-raw ! videoconvert ! queue ! x264enc tune=zerolatency ! mq.sink_1 mq.src_1 ! mux. src. ! audio/x-raw ! audioconvert ! queue ! opusenc ! mq.sink_0 mq.src_0 ! mux.
```

Interrupt with Ctrl+C, wait for EOS to propagate.

## Thanks

The development of this plugin was sponsored by PLAY Inc.
