#include <gst/video/video.h>
#include <gst/audio/audio.h>
#include <cdi_log_api.h>
#include <cdi_avm_api.h>
#include <cdi_baseline_profile_02_00_api.h>
#include <cdi_avm_payloads_api.h>
#include <gst-scte-104.h>
#include "aws-cdi-src.h"
#include "common.h"

/*
 * Locking order: inner source, outer source
 */

GST_DEBUG_CATEGORY_STATIC (gst_aws_cdi_src_debug);
#define GST_CAT_DEFAULT gst_aws_cdi_src_debug

enum
{
  PROP_0,
  PROP_ADDRESS,
  PROP_PORT,
};

#define parent_class gst_aws_cdi_src_inner_parent_class
G_DEFINE_TYPE (GstAwsCdiSrcInner, gst_aws_cdi_src_inner, GST_TYPE_PUSH_SRC);

#define DEFAULT_PORT 2000
#define DEFAULT_ADDRESS "0.0.0.0"

#define GST_ELEMENT_PROGRESS(el, type, code, text)      \
  G_STMT_START {                                          \
    gchar *__txt = _gst_element_error_printf text;        \
    gst_element_post_message (GST_ELEMENT_CAST (el),      \
        gst_message_new_progress (GST_OBJECT_CAST (el),   \
          GST_PROGRESS_TYPE_ ##type, code, __txt));     \
    g_free (__txt);                                       \
  } G_STMT_END

static GstStaticPadTemplate inner_src_template = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-raw,"
        "width = (int) [ 1, MAX ],"
        "height = (int) [ 1, MAX ],"
        "framerate = (fraction) [ 0, MAX ],"
        "format = (string) RGB,"
        "colorimetry = (string) 1:1:1:1,"
        "pixel-aspect-ratio = (fraction) 1/1," "interlace-mode=progressive,;"
        /* SMPTE ST 2110-30:2017 Table 1 */
        /* M (Mono) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=1, rate={48000, 96000};"
        /* ST (Standard Stereo) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=2, rate={48000, 96000};"
        /* DM (Dual Mono) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=2, channel-mask=(bitmask)0x0, rate={48000, 96000};"
        /* GStreamer has no notion of LtRt (Matrix Stereo) */
        /* 51 (5.1 Surround) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=6, channel-mask=(bitmask)0x3f, rate={48000, 96000};"
        /* 71 (7.1 Surround) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=8, channel-mask=(bitmask)0xc3f, rate={48000, 96000};"
        /* 222 (22.2 Surround) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=24, channel-mask=(bitmask)0xffffff, rate={48000, 96000};"
        /* TODO: SGRP (SDI audio group): would need a simple muxer and dedicated caps */
    )
    );

typedef struct
{
  GstBuffer *buffer;
  GstCaps *caps;
  GstTagList *tags;
} InputData;

static void
input_data_clear (InputData * data)
{
  gst_buffer_unref (data->buffer);
  if (data->caps) {
    gst_caps_unref (data->caps);
  }
  if (data->tags) {
    gst_tag_list_unref (data->tags);
  }
  memset (data, 0, sizeof (*data));
}

static void
drain_input_queue_unlocked (GstAwsCdiSrcInner * self)
{
  while (gst_queue_array_get_length (self->input_queue) > 0) {
    InputData *data =
        (InputData *) gst_queue_array_pop_head_struct (self->input_queue);
    input_data_clear (data);
  }
}

void
gst_aws_cdi_src_inner_finalize (GObject * object)
{
  GstAwsCdiSrcInner *self = GST_AWS_CDI_SRC_INNER (object);

  if (self->input_queue) {
    drain_input_queue_unlocked (self);
    gst_queue_array_free (self->input_queue);
    self->input_queue = NULL;
  }

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
TestConnectionCallback (const CdiCoreConnectionCbData * cb_data_ptr)
{
  GstAwsCdiSrc *src = GST_AWS_CDI_SRC (cb_data_ptr->connection_user_cb_param);

  if (cb_data_ptr->status_code == kCdiConnectionStatusConnected) {
    GST_INFO_OBJECT (src, "Connected");

    GST_OBJECT_LOCK (src);
    src->connection_status = cb_data_ptr->status_code;
    GST_OBJECT_UNLOCK (src);

    GST_ELEMENT_PROGRESS (src, COMPLETE, "connect", ("Connected sender"));
  } else {
    GST_OBJECT_LOCK (src);
    if (!src->stopping) {
      GST_OBJECT_UNLOCK (src);
      GST_ERROR_OBJECT (src, "Disconnected unexpectedly");
      GST_ELEMENT_ERROR (src, RESOURCE, OPEN_READ_WRITE,
          ("Disconnected."), (NULL));
    } else {
      GST_OBJECT_UNLOCK (src);
      GST_INFO_OBJECT (src, "Disconnected");
    }
  }
}

static GstCaps *
avm_audio_configuration_to_caps (GstObject * obj,
    const CdiAvmAudioConfig * audio_config)
{
  GstCaps *ret = NULL;
  GstAudioInfo ainfo;
  gint rate;
  gint channels;
  GstAudioChannelPosition position[64] = { GST_AUDIO_CHANNEL_POSITION_NONE, };

  switch (audio_config->sample_rate_khz) {
    case kCdiAvmAudioSampleRate48kHz:
      rate = 48000;
      break;
    case kCdiAvmAudioSampleRate96kHz:
      rate = 96000;
      break;
    default:
      g_assert_not_reached ();
      break;
  };

  switch (audio_config->grouping) {
    case kCdiAvmAudioM:
      channels = 1;
      gst_audio_channel_positions_from_mask (1, 0x1, position);
      break;
    case kCdiAvmAudioDM:
      channels = 2;
      gst_audio_channel_positions_from_mask (2, 0x0, position);
      break;
    case kCdiAvmAudioST:
      channels = 2;
      gst_audio_channel_positions_from_mask (2, 0x3, position);
      break;
    case kCdiAvmAudio51:
      channels = 6;
      gst_audio_channel_positions_from_mask (6, 0x3f, position);
      break;
    case kCdiAvmAudio71:
      channels = 8;
      gst_audio_channel_positions_from_mask (8, 0xc3f, position);
      break;
    case kCdiAvmAudio222:
      channels = 24;
      gst_audio_channel_positions_from_mask (24, 0xffffff, position);
      break;
    default:
      GST_ERROR_OBJECT (obj, "Unsupported audio grouping: %d",
          audio_config->grouping);
      goto done;
  }

  gst_audio_info_set_format (&ainfo,
      GST_AUDIO_FORMAT_S24BE, rate, channels, position);

  ret = gst_audio_info_to_caps (&ainfo);

done:
  return ret;
}

static GstCaps *
avm_video_configuration_to_caps (GstObject * obj,
    const CdiAvmVideoConfig * video_config)
{
  GstCaps *ret = NULL;
  GstVideoInfo vinfo;

  if (video_config->depth != kCdiAvmVidBitDepth8) {
    GST_ERROR_OBJECT (obj, "Non-8-bit depth formats are not supported");
    goto done;
  }

  if (video_config->alpha_channel != kCdiAvmAlphaUnused) {
    GST_ERROR_OBJECT (obj, "Alpha formats are not supported");
    goto done;
  }

  if (video_config->sampling != kCdiAvmVidRGB) {
    GST_ERROR_OBJECT (obj, "Non-RGB formats are not supported");
    goto done;
  }

  if (video_config->colorimetry != kCdiAvmVidColorimetryBT709) {
    GST_ERROR_OBJECT (obj, "Non-BT709 formats are not supported");
    goto done;
  }

  if (video_config->tcs != kCdiAvmVidTcsSDR) {
    GST_ERROR_OBJECT (obj, "Non-SDR formats are not supported");
    goto done;
  }

  if (video_config->range != kCdiAvmVidRangeFull) {
    GST_ERROR_OBJECT (obj, "Narrow range formats are not supported");
    goto done;
  }

  if (video_config->interlace != false) {
    GST_ERROR_OBJECT (obj, "Interlaced formats are not supported");
    goto done;
  }

  if (video_config->segmented != false) {
    GST_ERROR_OBJECT (obj, "Segmented formats are not supported");
    goto done;
  }

  if (video_config->par_width != 1 || video_config->par_height != 1) {
    GST_ERROR_OBJECT (obj, "Non 1/1 PAR is not supported");
    goto done;
  }

  if (video_config->start_vertical_pos != 0 ||
      video_config->start_horizontal_pos != 0 ||
      video_config->vertical_size != 0 || video_config->horizontal_size != 0) {
    GST_ERROR_OBJECT (obj, "Partial frames are not supported");
    goto done;
  }

  gst_video_info_set_format (&vinfo,
      GST_VIDEO_FORMAT_RGB, video_config->width, video_config->height);

  vinfo.colorimetry.transfer = GST_VIDEO_TRANSFER_GAMMA10;

  ret = gst_video_info_to_caps (&vinfo);

done:
  return ret;
}

static GstCaps *
avm_baseline_configuration_to_caps (GstObject * obj,
    const CdiAvmBaselineConfig * baseline_config)
{
  GstCaps *ret = NULL;

  switch (baseline_config->payload_type) {
    case kCdiAvmVideo:
      ret =
          avm_video_configuration_to_caps (obj, &baseline_config->video_config);
      break;
    case kCdiAvmAudio:
      ret =
          avm_audio_configuration_to_caps (obj, &baseline_config->audio_config);
      break;
    default:
      GST_ERROR_OBJECT (obj, "Unsupported CDI payload type");
      break;
  }

  return ret;
}

static gboolean
gst_aws_cdi_src_inner_stop (GstBaseSrc * bsrc)
{
  GstAwsCdiSrcInner *self = GST_AWS_CDI_SRC_INNER_CAST (bsrc);

  gst_clear_caps (&self->received_config);

  GST_OBJECT_LOCK (self);
  drain_input_queue_unlocked (self);
  GST_OBJECT_UNLOCK (self);

  return TRUE;
}

static GstFlowReturn
gst_aws_cdi_src_inner_create (GstPushSrc * bsrc, GstBuffer ** buffer)
{
  GstAwsCdiSrcInner *src = GST_AWS_CDI_SRC_INNER (bsrc);
  InputData *data;
  GstCaps *caps = NULL;
  GstTagList *tags = NULL;

  GST_OBJECT_LOCK (src);
  if (src->unlock) {
    GST_OBJECT_UNLOCK (src);
    return GST_FLOW_FLUSHING;
  }

  while (gst_queue_array_is_empty (src->input_queue)) {
    g_cond_wait (&src->cond, GST_OBJECT_GET_LOCK (src));

    if (src->unlock) {
      GST_OBJECT_UNLOCK (src);
      return GST_FLOW_FLUSHING;
    }
  }

  data = (InputData *) gst_queue_array_pop_head_struct (src->input_queue);

  if (data->caps) {
    caps = gst_caps_ref (data->caps);
  }

  if (data->tags) {
    tags = gst_tag_list_ref (data->tags);
  }

  *buffer = gst_buffer_ref (data->buffer);
  input_data_clear (data);

  GST_OBJECT_UNLOCK (src);

  if (caps) {
    gst_base_src_set_caps (GST_BASE_SRC (bsrc), caps);
    gst_caps_unref (caps);
  }

  if (tags) {
    gst_pad_push_event (GST_BASE_SRC_PAD (bsrc), gst_event_new_tag (tags));
  }

  return GST_FLOW_OK;
}

static gboolean
gst_aws_cdi_src_inner_unlock (GstBaseSrc * bsrc)
{
  GstAwsCdiSrcInner *self = GST_AWS_CDI_SRC_INNER_CAST (bsrc);

  GST_OBJECT_LOCK (self);
  self->unlock = TRUE;
  g_cond_signal (&self->cond);
  GST_OBJECT_UNLOCK (self);

  return TRUE;
}

static gboolean
gst_aws_cdi_src_inner_unlock_stop (GstBaseSrc * bsrc)
{
  GstAwsCdiSrcInner *self = GST_AWS_CDI_SRC_INNER_CAST (bsrc);

  GST_OBJECT_LOCK (self);
  self->unlock = FALSE;
  drain_input_queue_unlocked (self);
  GST_OBJECT_UNLOCK (self);

  return TRUE;
}


static gboolean
gst_aws_cdi_src_inner_query (GstBaseSrc * bsrc, GstQuery * query)
{
  GstAwsCdiSrcInner *src = GST_AWS_CDI_SRC_INNER (bsrc);

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CAPS:
      if (src->received_config) {
        gst_query_set_caps_result (query, src->received_config);
        return TRUE;
      }
      break;
    default:
      break;
  }

  return GST_BASE_SRC_CLASS (gst_aws_cdi_src_inner_parent_class)->query (bsrc,
      query);
}

static void
gst_aws_cdi_src_inner_class_init (GstAwsCdiSrcInnerClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GstBaseSrcClass *basesrc_class = GST_BASE_SRC_CLASS (klass);
  GstPushSrcClass *pushsrc_class = GST_PUSH_SRC_CLASS (klass);

  gobject_class->finalize = gst_aws_cdi_src_inner_finalize;

  basesrc_class->negotiate = NULL;
  basesrc_class->stop = GST_DEBUG_FUNCPTR (gst_aws_cdi_src_inner_stop);
  basesrc_class->unlock = GST_DEBUG_FUNCPTR (gst_aws_cdi_src_inner_unlock);
  basesrc_class->unlock_stop =
      GST_DEBUG_FUNCPTR (gst_aws_cdi_src_inner_unlock_stop);
  basesrc_class->query = GST_DEBUG_FUNCPTR (gst_aws_cdi_src_inner_query);

  pushsrc_class->create = GST_DEBUG_FUNCPTR (gst_aws_cdi_src_inner_create);

  gst_element_class_add_static_pad_template (element_class,
      &inner_src_template);

  gst_element_class_set_static_metadata (element_class, "AWS CDI Source",
      "Video/Source", "AWS CDI Source",
      "Mathieu Duponchelle <mathieu@centricular.com>");
}

static void
gst_aws_cdi_src_inner_init (GstAwsCdiSrcInner * self)
{
  gst_base_src_set_live (GST_BASE_SRC (self), TRUE);
  gst_base_src_set_format (GST_BASE_SRC (self), GST_FORMAT_TIME);

  /* TODO: expose buffer size property */
  self->input_queue = gst_queue_array_new_for_struct (sizeof (InputData), 1);
}

/* Outer bin */

G_DEFINE_TYPE (GstAwsCdiSrc, gst_aws_cdi_src, GST_TYPE_BIN);
GST_ELEMENT_REGISTER_DEFINE (aws_cdi_src, "awscdisrc", GST_RANK_NONE,
    GST_TYPE_AWS_CDI_SRC);

static GstStaticPadTemplate src_template = GST_STATIC_PAD_TEMPLATE ("src_%u",
    GST_PAD_SRC,
    GST_PAD_SOMETIMES,
    GST_STATIC_CAPS ("video/x-raw,"
        "width = (int) [ 1, MAX ],"
        "height = (int) [ 1, MAX ],"
        "framerate = (fraction) [ 0, MAX ],"
        "format = (string) RGB,"
        "colorimetry = (string) 1:1:1:1,"
        "pixel-aspect-ratio = (fraction) 1/1," "interlace-mode=progressive,;"
        /* SMPTE ST 2110-30:2017 Table 1 */
        /* M (Mono) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=1, rate={48000, 96000};"
        /* ST (Standard Stereo) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=2, rate={48000, 96000};"
        /* DM (Dual Mono) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=2, channel-mask=(bitmask)0x0, rate={48000, 96000};"
        /* GStreamer has no notion of LtRt (Matrix Stereo) */
        /* 51 (5.1 Surround) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=6, channel-mask=(bitmask)0x3f, rate={48000, 96000};"
        /* 71 (7.1 Surround) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=8, channel-mask=(bitmask)0xc3f, rate={48000, 96000};"
        /* 222 (22.2 Surround) */
        "audio/x-raw, format=S24BE, layout=interleaved, channels=24, channel-mask=(bitmask)0xffffff, rate={48000, 96000};"
        /* TODO: SGRP (SDI audio group): would need a simple muxer and dedicated caps */
    )
    );

static void
UnpacketizeAncCb (void *user_data_ptr, CdiFieldKind field_kind,
    const CdiAvmAncillaryDataPacket * packet_ptr, bool has_parity_error,
    bool has_checksum_error)
{
  GstAwsCdiSrc *src = (GstAwsCdiSrc *) user_data_ptr;

  if (packet_ptr != NULL) {
    GST_TRACE_OBJECT (src, "Unpacketized one ancillary data packet");
    gst_queue_array_push_tail_struct (src->vanc_queue, (gpointer) packet_ptr);
  }
}

static void
TestAvmRxCallback (const CdiAvmRxCbData * cb_data_ptr)
{
  GstMapInfo map;
  InputData data = { 0 };
  GstAwsCdiSrc *self = (GstAwsCdiSrc *) cb_data_ptr->core_cb_data.user_cb_param;
  GstClockTime arrival_time =
      gst_element_get_current_running_time (GST_ELEMENT (self));
  GstCaps *caps = NULL;
  uint16_t stream_id = cb_data_ptr->avm_extra_data.stream_identifier;
  GstAwsCdiSrcInner *inner_source = NULL;
  GstPad *pad = NULL;
  GstTagList *tags = NULL;

  GST_OBJECT_LOCK (self);

  if (stream_id == self->meta_stream) {
    CdiReturnStatus rs =
        CdiAvmUnpacketizeAncillaryData (&cb_data_ptr->sgl, UnpacketizeAncCb,
        self);

    if (rs != kCdiStatusOk) {
      GST_WARNING_OBJECT (self, "Failed to unpacketize ancillary data: %s",
          CdiCoreStatusToString (rs));
    }

    GST_OBJECT_UNLOCK (self);
    goto done_unlocked;
  }

  inner_source =
      g_hash_table_lookup (self->inner_sources, GINT_TO_POINTER (stream_id));

  if (!inner_source) {
    CdiAvmBaselineConfig baseline_config = { 0, };
    CdiReturnStatus rc;
    if (cb_data_ptr->config_ptr == NULL) {
      GST_ERROR_OBJECT (self,
          "Discarding initial buffer without a configuration");
      GST_OBJECT_UNLOCK (self);
      goto done_unlocked;
    }

    rc = CdiAvmParseBaselineConfiguration (cb_data_ptr->config_ptr,
        &baseline_config);

    if (rc != kCdiStatusOk) {
      GST_ERROR_OBJECT (self, "Failed to parse baseline configuration: %s",
          CdiCoreStatusToString (rc));
      GST_OBJECT_UNLOCK (self);
      goto done_unlocked;
    }

    if (baseline_config.payload_type == kCdiAvmAncillary) {
      CdiReturnStatus rs =
          CdiAvmUnpacketizeAncillaryData (&cb_data_ptr->sgl, UnpacketizeAncCb,
          self);

      GST_INFO_OBJECT (self, "Identified meta stream with ID %u", stream_id);

      if (rs != kCdiStatusOk) {
        GST_WARNING_OBJECT (self, "Failed to unpacketize ancillary data: %s",
            CdiCoreStatusToString (rs));
      }

      self->meta_stream = stream_id;

      GST_OBJECT_UNLOCK (self);
      goto done_unlocked;
    }

    inner_source = g_object_new (GST_TYPE_AWS_CDI_SRC_INNER, NULL);

    g_hash_table_insert (self->inner_sources, GINT_TO_POINTER (stream_id),
        inner_source);

    if (baseline_config.payload_type == kCdiAvmVideo) {
      inner_source->is_video = TRUE;
    }

    pad = gst_element_get_static_pad (GST_ELEMENT (inner_source), "src");
  }

  GST_OBJECT_UNLOCK (self);

  GST_OBJECT_LOCK (inner_source);

  if (NULL != cb_data_ptr->config_ptr) {
    CdiAvmBaselineConfig baseline_config = { 0, };
    CdiReturnStatus rc =
        CdiAvmParseBaselineConfiguration (cb_data_ptr->config_ptr,
        &baseline_config);

    if (kCdiStatusOk == rc) {
      if (!(caps =
              avm_baseline_configuration_to_caps (GST_OBJECT (self),
                  &baseline_config))) {
        goto done;
      }

      if (baseline_config.payload_type == kCdiAvmAudio &&
          *(baseline_config.audio_config.language)) {
        guint n_chars = 0;
        gchar *language = NULL;

        while (*(baseline_config.audio_config.language + n_chars)) {
          n_chars++;
        }

        language = g_malloc0 (n_chars + 1);

        strncpy (language, baseline_config.audio_config.language, n_chars);

        tags = gst_tag_list_new (GST_TAG_LANGUAGE_CODE, language, NULL);

        g_free (language);
      }

      /* TODO: allow reconfiguration */
      if (inner_source->received_config
          && !gst_caps_is_equal (inner_source->received_config, caps)) {
        GST_OBJECT_UNLOCK (inner_source);
        GST_ERROR_OBJECT (inner_source,
            "Received new configuration but renegotiation is not supported");
        GST_ELEMENT_ERROR (inner_source, CORE, NEGOTIATION,
            ("Renegotiation is not supported"),
            ("Received new configuration but renegotiation is not supported"));
        goto done_unlocked;
      }

      inner_source->received_config = gst_caps_ref (caps);
    } else {
      GST_ERROR_OBJECT (self, "Failed to parse baseline configuration: %s",
          CdiCoreStatusToString (rc));
      goto done;
    }
  } else if (!inner_source->received_config) {
    GST_ERROR_OBJECT (self,
        "Discarding initial buffer without a configuration");
    goto done;
  }

  if (pad) {
    GstPad *gpad;
    gchar *ghostpad_name = g_strdup_printf ("src_%u", stream_id);;

    GST_OBJECT_UNLOCK (inner_source);
    gst_bin_add (GST_BIN (self), GST_ELEMENT (inner_source));

    pad = gst_element_get_static_pad (GST_ELEMENT (inner_source), "src");
    gpad = gst_ghost_pad_new (ghostpad_name, pad);
    gst_pad_set_active (gpad, TRUE);
    gst_element_add_pad (GST_ELEMENT (self), gpad);
    g_free (ghostpad_name);
    gst_object_unref (pad);

    gst_element_sync_state_with_parent (GST_ELEMENT (inner_source));

    GST_OBJECT_LOCK (inner_source);
  }

  data.buffer =
      gst_buffer_new_allocate (NULL, cb_data_ptr->sgl.total_data_size, NULL);

  data.caps = caps;

  data.tags = tags;

  /* Set an arrival time on the output buffer */
  GST_BUFFER_DTS (data.buffer) = arrival_time;

  gst_buffer_map (data.buffer, &map, GST_MAP_WRITE);

  if (inner_source->is_video) {
    GST_OBJECT_LOCK (self);
    while (gst_queue_array_get_length (self->vanc_queue) > 0) {
      CdiAvmAncillaryDataPacket *packet_ptr = (CdiAvmAncillaryDataPacket *)
          gst_queue_array_pop_head_struct (self->vanc_queue);

      if (packet_ptr->did == 0x41 && packet_ptr->sdid == 0x07) {
        guint8 *scte_104_data = g_malloc (packet_ptr->data_count);

        memcpy (scte_104_data, packet_ptr->user_data, packet_ptr->data_count);
        GST_TRACE_OBJECT (inner_source, "Adding one SCTE 104 meta");
        gst_buffer_add_scte_104_meta (data.buffer, scte_104_data,
            packet_ptr->data_count);
      }
    }

    GST_OBJECT_UNLOCK (self);
  }

  CdiCoreGather (&cb_data_ptr->sgl, 0, map.data,
      cb_data_ptr->sgl.total_data_size);

  gst_buffer_unmap (data.buffer, &map);

  gst_queue_array_push_tail_struct (inner_source->input_queue, &data);
  g_cond_signal (&inner_source->cond);

done:
  GST_OBJECT_UNLOCK (inner_source);

done_unlocked:
  CdiCoreRxFreeBuffer (&cb_data_ptr->sgl);
}

static gboolean
gst_aws_cdi_src_start (GstAwsCdiSrc * src)
{
  gboolean ret = FALSE;
  CdiReturnStatus rs = kCdiStatusOk;

  src->stopping = FALSE;
  src->meta_stream = -1;

  GST_ELEMENT_PROGRESS (src, START, "connect", ("Connecting sender"));

  CdiLogMethodData log_method_data = {
    .log_method = kLogMethodStdout
  };
  rs = gst_aws_ensure_cdi_initialized ();
  if (kCdiStatusOk != rs) {
    GST_ERROR_OBJECT (src,
        "SDK core initialize failed. Error=[%d], Message=[%s]", rs,
        CdiCoreStatusToString (rs));
  }

  if (kCdiStatusOk == rs) {
    CdiAdapterData adapter_data = {
      .adapter_ip_addr_str = src->address,
      .adapter_type = kCdiAdapterTypeEfa,
    };
    rs = CdiCoreNetworkAdapterInitialize (&adapter_data, &src->adapter_handle);
  }

  if (kCdiStatusOk == rs) {
    CdiRxConfigData config_data = {
      .adapter_handle = src->adapter_handle,
      .dest_port = src->port,
      .shared_thread_id = 0,
      .thread_core_num = -1,
      .rx_buffer_type = kCdiSgl,
      .linear_buffer_size = 0,
      .user_cb_param = src,
      .connection_name_str = NULL,
      .connection_log_method_data_ptr = &log_method_data,
      .connection_cb_ptr = TestConnectionCallback,
      .connection_user_cb_param = src,
      .stats_cb_ptr = NULL,
      .stats_user_cb_param = NULL,
      .stats_config.stats_period_seconds = 0,
      .stats_config.disable_cloudwatch_stats = true
    };

    rs = CdiAvmRxCreate (&config_data, TestAvmRxCallback,
        &src->connection_handle);
  }

  if (rs == kCdiStatusOk) {
    GST_INFO_OBJECT (src, "Created connection %p", src->connection_handle);
  }

  return rs == kCdiStatusOk;
  ret = TRUE;

  return ret;
}

static void
gst_aws_cdi_src_stop (GstAwsCdiSrc * self)
{
  GST_OBJECT_LOCK (self);
  self->stopping = TRUE;
  GST_OBJECT_UNLOCK (self);

  if (self->connection_handle) {
    GST_INFO_OBJECT (self, "Closing connection %p", self->connection_handle);

    CdiCoreConnectionDestroy (self->connection_handle);
    self->connection_handle = NULL;
  }

  if (self->adapter_handle) {
    CdiCoreNetworkAdapterDestroy (self->adapter_handle);
    self->adapter_handle = NULL;
  }
}

static GstStateChangeReturn
gst_aws_cdi_src_change_state (GstElement * element, GstStateChange transition)
{
  GstAwsCdiSrc *self = GST_AWS_CDI_SRC (element);
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;

  GST_DEBUG_OBJECT (self, "Changing state: %s => %s",
      gst_element_state_get_name (GST_STATE_TRANSITION_CURRENT (transition)),
      gst_element_state_get_name (GST_STATE_TRANSITION_NEXT (transition)));

  ret =
      GST_ELEMENT_CLASS (gst_aws_cdi_src_parent_class)->change_state (element,
      transition);

  if (ret == GST_STATE_CHANGE_FAILURE)
    goto done;

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      if (gst_aws_cdi_src_start (self) == FALSE)
        ret = GST_STATE_CHANGE_FAILURE;
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      ret = GST_STATE_CHANGE_NO_PREROLL;
      break;
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      ret = GST_STATE_CHANGE_NO_PREROLL;
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      gst_aws_cdi_src_stop (self);
      break;
    default:
      break;
  }

done:
  return ret;
}

static void
gst_aws_cdi_src_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstAwsCdiSrc *src = GST_AWS_CDI_SRC (object);

  switch (property_id) {
    case PROP_PORT:
      GST_OBJECT_LOCK (src);
      src->port = g_value_get_int (value);
      GST_OBJECT_UNLOCK (src);
      break;
    case PROP_ADDRESS:
      GST_OBJECT_LOCK (src);
      src->address = g_value_dup_string (value);
      GST_OBJECT_UNLOCK (src);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
gst_aws_cdi_src_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstAwsCdiSrc *src = GST_AWS_CDI_SRC (object);

  switch (property_id) {
    case PROP_PORT:
      GST_OBJECT_LOCK (src);
      g_value_set_int (value, src->port);
      GST_OBJECT_UNLOCK (src);
      break;
    case PROP_ADDRESS:
      GST_OBJECT_LOCK (src);
      g_value_set_string (value, src->address);
      GST_OBJECT_UNLOCK (src);
      break;
  }
}

static void
gst_aws_cdi_src_finalize (GObject * object)
{
  GstAwsCdiSrc *self = GST_AWS_CDI_SRC (object);

  g_free (self->address);
  g_hash_table_unref (self->inner_sources);
  gst_queue_array_free (self->vanc_queue);

  G_OBJECT_CLASS (gst_aws_cdi_src_parent_class)->finalize (object);
}

static void
gst_aws_cdi_src_class_init (GstAwsCdiSrcClass * klass)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->finalize = gst_aws_cdi_src_finalize;
  gobject_class->set_property = gst_aws_cdi_src_set_property;
  gobject_class->get_property = gst_aws_cdi_src_get_property;
  element_class->change_state = gst_aws_cdi_src_change_state;

  g_object_class_install_property (gobject_class, PROP_ADDRESS,
      g_param_spec_string ("address", "Address",
          "The address to receive on",
          DEFAULT_ADDRESS,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
              GST_PARAM_MUTABLE_READY | G_PARAM_CONSTRUCT)));

  g_object_class_install_property (gobject_class, PROP_PORT,
      g_param_spec_int ("port", "Port",
          "The port to receive on", 1, G_MAXUINT16,
          DEFAULT_PORT,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
              GST_PARAM_MUTABLE_READY | G_PARAM_CONSTRUCT)));

  gst_element_class_add_static_pad_template (element_class, &src_template);

  gst_element_class_set_static_metadata (element_class, "AWS CDI Source",
      "Video/Source", "AWS CDI Source",
      "Mathieu Duponchelle <mathieu@centricular.com>");

  GST_DEBUG_CATEGORY_INIT (gst_aws_cdi_src_debug, "awscdisrc",
      0, "debug category for awscdisrc element");
}

static void
gst_aws_cdi_src_init (GstAwsCdiSrc * self)
{
  GST_OBJECT_FLAG_SET (GST_OBJECT (self), GST_ELEMENT_FLAG_SOURCE);

  self->inner_sources = g_hash_table_new (g_direct_hash, g_direct_equal);
  self->vanc_queue =
      gst_queue_array_new_for_struct (sizeof (CdiAvmAncillaryDataPacket), 1);
}
