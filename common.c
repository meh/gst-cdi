#include <glib.h>
#include <cdi_log_api.h>
#include "common.h"

CdiReturnStatus
gst_aws_ensure_cdi_initialized (void)
{
  static gsize once = 0;
  static CdiReturnStatus rs = kCdiStatusOk;

  if (g_once_init_enter (&once)) {
    CdiLogMethodData log_method_data = {
      .log_method = kLogMethodStdout
    };
    CdiCoreConfigData core_config = {
      .default_log_level = kLogDebug,
      .global_log_method_data_ptr = &log_method_data,
      .cloudwatch_config_ptr = NULL
    };
    rs = CdiCoreInitialize (&core_config);
    g_once_init_leave (&once, 1);
  }

  return rs;
}
