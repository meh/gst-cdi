/*
 * Copyright (c) 2016-2017 Kernel Labs Inc. All Rights Reserved
 *
 * Address: Kernel Labs Inc., PO Box 745, St James, NY. 11780
 * Contact: sales@kernellabs.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/* References:
 * ST2010-2008 "Vertical Ancillary Data Mapping of ANSI/SCTE 104 Messages"
 * ANSI/SCTE104 2019a
 */

#include <gst/gst.h>
#include <gst/base/gstbitwriter.h>
#include <gst/base/gstbitreader.h>

#include "gst-scte-104.h"

struct _GstSCTE104PacketParser
{
  guint8 payload[GST_SCTE_104_PACKET_MAX_PAYLOAD_LENGTH];
  guint16 payload_length;
};

static guint8 *
parse_splice_request_data (guint8 * p, GstSCTE104SpliceRequestData * d)
{
  d->splice_insert_type = *(p++);
  d->splice_event_id =
      *(p + 0) << 24 | *(p + 1) << 16 | *(p + 2) << 8 | *(p + 3);
  p += 4;
  d->unique_program_id = *(p + 0) << 8 | *(p + 1);
  p += 2;
  d->pre_roll_time = *(p + 0) << 8 | *(p + 1);
  p += 2;
  d->brk_duration = *(p + 0) << 8 | *(p + 1);
  p += 2;
  d->avail_num = *(p++);
  d->avails_expected = *(p++);
  d->auto_return_flag = *(p++);

  /* TODO: We don't support splice cancel, but we'll pass it through with a warning. */
  switch (d->splice_insert_type) {
    case SPLICESTART_IMMEDIATE:
    case SPLICEEND_IMMEDIATE:
    case SPLICESTART_NORMAL:
    case SPLICEEND_NORMAL:
    case SPLICE_CANCEL:
      break;
    default:
      /* We don't support this splice command */
      GST_ERROR ("splice_insert_type 0x%x", d->splice_insert_type);
  }

  return p;
}

static void
gen_splice_request_data (const GstSCTE104SpliceRequestData * d,
    GstBitWriter * bw)
{
  gst_bit_writer_put_bits_uint32 (bw, d->splice_insert_type, 8);
  gst_bit_writer_put_bits_uint32 (bw, d->splice_event_id, 32);
  gst_bit_writer_put_bits_uint16 (bw, d->unique_program_id, 16);
  gst_bit_writer_put_bits_uint16 (bw, d->pre_roll_time, 16);
  gst_bit_writer_put_bits_uint16 (bw, d->brk_duration, 16);
  gst_bit_writer_put_bits_uint8 (bw, d->avail_num, 8);
  gst_bit_writer_put_bits_uint8 (bw, d->avails_expected, 8);
  gst_bit_writer_put_bits_uint8 (bw, d->auto_return_flag, 8);
}

static guint8 *
parse_time_signal_request_data (guint8 * p, GstSCTE104TimeSignalRequestData * d)
{
  d->pre_roll_time = *(p + 0) << 8 | *(p + 1);
  p += 2;
  return p;
}

static void
gen_time_signal_request_data (const GstSCTE104TimeSignalRequestData * d,
    GstBitWriter * bw)
{
  gst_bit_writer_put_bits_uint16 (bw, d->pre_roll_time, 16);
}

static guint8 *
parse_descriptor_request_data (guint8 * p,
    GstSCTE104InsertDescriptorRequestData * d, guint32 descriptor_size)
{
  d->descriptor_count = *(p++);
  d->total_length = descriptor_size;

  memset (d->descriptor_bytes, 0, sizeof (d->descriptor_bytes));
  if (d->total_length < sizeof (d->descriptor_bytes)) {
    memcpy (d->descriptor_bytes, p, d->total_length);
  }
  p += d->total_length;

  return p;
}

static void
gen_descriptor_request_data (const GstSCTE104InsertDescriptorRequestData * d,
    GstBitWriter * bw)
{
  gst_bit_writer_put_bits_uint32 (bw, d->descriptor_count, 8);
  for (guint i = 0; i < d->total_length; i++)
    gst_bit_writer_put_bits_uint8 (bw, d->descriptor_bytes[i], 8);
}


static guint8 *
parse_dtmf_request_data (guint8 * p, GstSCTE104DTMFDescriptorRequestData * d)
{
  d->pre_roll_time = *(p++);
  d->dtmf_length = *(p++);
  memset (d->dtmf_char, 0, sizeof (d->dtmf_char));
  if (d->dtmf_length <= sizeof (d->dtmf_char)) {
    memcpy (d->dtmf_char, p, d->dtmf_length);
  }
  p += d->dtmf_length;

  return p;
}

static void
gen_dtmf_request_data (const GstSCTE104DTMFDescriptorRequestData * d,
    GstBitWriter * bw)
{

  gst_bit_writer_put_bits_uint8 (bw, d->pre_roll_time, 8);
  gst_bit_writer_put_bits_uint32 (bw, d->dtmf_length, 8);
  for (guint i = 0; i < d->dtmf_length; i++)
    gst_bit_writer_put_bits_uint8 (bw, d->dtmf_char[i], 8);
}

static guint8 *
parse_avail_request_data (guint8 * p, GstSCTE104AvailDescriptorRequestData * d)
{
  d->num_provider_avails = *(p++);
  memset (d->provider_avail_id, 0, sizeof (d->provider_avail_id));

  for (guint i = 0; i < d->num_provider_avails; i++) {
    d->provider_avail_id[i] =
        *(p + 0) << 24 | *(p + 1) << 16 | *(p + 2) << 8 | *(p + 3);
    p += 4;
  }

  return p;
}

static void
gen_avail_request_data (const GstSCTE104AvailDescriptorRequestData * d,
    GstBitWriter * bw)
{
  gst_bit_writer_put_bits_uint32 (bw, d->num_provider_avails, 8);
  for (guint i = 0; i < d->num_provider_avails; i++)
    gst_bit_writer_put_bits_uint32 (bw, d->provider_avail_id[i], 32);
}

static guint8 *
parse_segmentation_request_data (guint8 * p,
    GstSCTE104SegmentationDescriptorRequestData * d)
{
  d->event_id = *(p + 0) << 24 | *(p + 1) << 16 | *(p + 2) << 8 | *(p + 3);
  p += 4;
  d->event_cancel_indicator = *(p++);
  d->duration = (p[0] << 8) | p[1];
  p += 2;
  d->upid_type = *(p++);
  d->upid_length = *(p++);

  memset (d->upid, 0, sizeof (d->upid));

  if (d->upid_length > sizeof (d->upid))
    return NULL;
  memcpy (d->upid, p, d->upid_length);
  p += d->upid_length;
  d->type_id = *(p++);
  d->segment_num = *(p++);
  d->segments_expected = *(p++);
  d->duration_extension_frames = *(p++);
  d->delivery_not_restricted_flag = *(p++);
  d->web_delivery_allowed_flag = *(p++);
  d->no_regional_blackout_flag = *(p++);
  d->archive_allowed_flag = *(p++);
  d->device_restrictions = *(p++);

  return p;
}

static void
gen_segmentation_request_data (const GstSCTE104SegmentationDescriptorRequestData
    * d, GstBitWriter * bw)
{
  gst_bit_writer_put_bits_uint32 (bw, d->event_id, 32);
  gst_bit_writer_put_bits_uint32 (bw, d->event_cancel_indicator, 8);
  gst_bit_writer_put_bits_uint32 (bw, d->duration, 16);
  gst_bit_writer_put_bits_uint32 (bw, d->upid_type, 8);
  gst_bit_writer_put_bits_uint32 (bw, d->upid_length, 8);

  for (guint i = 0; i < d->upid_length; i++)
    gst_bit_writer_put_bits_uint8 (bw, d->upid[i], 8);

  gst_bit_writer_put_bits_uint32 (bw, d->type_id, 8);
  gst_bit_writer_put_bits_uint32 (bw, d->segment_num, 8);
  gst_bit_writer_put_bits_uint32 (bw, d->segments_expected, 8);
  gst_bit_writer_put_bits_uint32 (bw, d->duration_extension_frames, 8);
  gst_bit_writer_put_bits_uint32 (bw, d->delivery_not_restricted_flag, 8);
  gst_bit_writer_put_bits_uint32 (bw, d->web_delivery_allowed_flag, 8);
  gst_bit_writer_put_bits_uint32 (bw, d->no_regional_blackout_flag, 8);
  gst_bit_writer_put_bits_uint32 (bw, d->archive_allowed_flag, 8);
  gst_bit_writer_put_bits_uint32 (bw, d->device_restrictions, 8);
}

static guint8 *
parse_proprietary_command_request_data (guint8 * p,
    GstSCTE104ProprietaryCommandRequestData * d, guint32 descriptor_size)
{
  memset (d->proprietary_data, 0, sizeof (d->proprietary_data));

  d->proprietary_id =
      *(p + 0) << 24 | *(p + 1) << 16 | *(p + 2) << 8 | *(p + 3);
  p += 4;
  d->proprietary_command = *(p++);
  d->data_length = descriptor_size - 5;

  memcpy (d->proprietary_data, p, d->data_length);
  p += d->data_length;

  return p;
}

static void
gen_proprietary_command_request_data (const
    GstSCTE104ProprietaryCommandRequestData * d, GstBitWriter * bw)
{
  gst_bit_writer_put_bits_uint32 (bw, d->proprietary_id, 32);
  gst_bit_writer_put_bits_uint32 (bw, d->proprietary_command, 8);

  for (guint i = 0; i < d->data_length; i++)
    gst_bit_writer_put_bits_uint8 (bw, d->proprietary_data[i], 8);
}

static guint8 *
parse_tier_data (guint8 * p, GstSCTE104TierData * d)
{
  d->tier_data = *(p + 0) << 8 | *(p + 1);
  p += 2;

  /* SCTE 104:2015 Sec 9.8.9.1 says the top four bits must be zero */
  d->tier_data &= 0x0fff;

  return p;
}

static void
gen_tier_data (const GstSCTE104TierData * d, GstBitWriter * bw)
{
  /* SCTE 104:2015 Sec 9.8.9.1 says the top four bits must be zero */
  gst_bit_writer_put_bits_uint16 (bw, d->tier_data & 0x0fff, 16);
}

static guint8 *
parse_time_descriptor (guint8 * p, GstSCTE104TimeDescriptorData * d)
{
  GstBitReader br = GST_BIT_READER_INIT (p, 12);

  gst_bit_reader_get_bits_uint64 (&br, &d->tai_seconds, 48);
  gst_bit_reader_get_bits_uint32 (&br, &d->tai_ns, 32);
  gst_bit_reader_get_bits_uint32 (&br, &d->utc_offset, 16);

  return p;
}

static void
gen_time_descriptor (const GstSCTE104TimeDescriptorData * d, GstBitWriter * bw)
{
  gst_bit_writer_put_bits_uint64 (bw, d->tai_seconds, 48);
  gst_bit_writer_put_bits_uint32 (bw, d->tai_ns, 32);
  gst_bit_writer_put_bits_uint32 (bw, d->utc_offset, 16);
}


static const guint8 *
parse_mom_timestamp (const guint8 * p,
    GstSCTE104MultipleOperationTimestamp * ts)
{
  ts->time_type = *(p++);
  switch (ts->time_type) {
    case 1:
      ts->time_type_1.utc_seconds =
          *(p + 0) << 24 | *(p + 1) << 16 | *(p + 2) << 8 | *(p + 3);
      ts->time_type_1.utc_microseconds = *(p + 4) << 8 | *(p + 5);
      p += 6;
      break;
    case 2:
      ts->time_type_2.hours = *(p + 0);
      ts->time_type_2.minutes = *(p + 1);
      ts->time_type_2.seconds = *(p + 2);
      ts->time_type_2.frames = *(p + 3);
      p += 4;
      break;
    case 3:
      ts->time_type_3.gpi_number = *(p + 0);
      ts->time_type_3.gpi_edge = *(p + 1);
      p += 2;
      break;
    case 0:
      /* The spec says no time is defined, this is a legitimate state. */
      break;
    default:
      GST_ERROR ("unsupported time_type 0x%x, assuming no time.",
          ts->time_type);
  }

  return p;
}

void
gst_scte_104_packet_init (GstSCTE104Packet * pkt, guint16 op_id)
{
  pkt->payload_descriptor_byte = 0x08;
  pkt->version = 0x08;
  pkt->continued_pkt = 0;
  pkt->following_pkt = 0;
  pkt->duplicate_msg = 0;

  /* Note, we take advantage of the SOM OpID field even with
     Multiple Operation Messages */
  pkt->so_msg.op_id = op_id;
  if (op_id == 0xffff) {
    /* Set some reasonable defaults for the MOM properties */
    pkt->mo_msg.rsvd = 0xffff;
  }
}

void
gst_scte_104_packet_unset (GstSCTE104Packet * pkt)
{
  GstSCTE104MultipleOperation *m;

  m = &pkt->mo_msg;
  for (guint i = 0; i < m->num_ops; i++) {
    free (m->ops[i].data);
  }
  free (m->ops);
}

static gboolean
append_payload (GstSCTE104PacketParser * parser,
    const guint8 * payload, guint16 payload_length)
{
  if (parser->payload_length + payload_length >
      GST_SCTE_104_PACKET_MAX_PAYLOAD_LENGTH) {
    GST_ERROR ("Parser overflow");
    parser->payload_length = 0;
    return FALSE;
  }

  memcpy (parser->payload + parser->payload_length, payload, payload_length);

  parser->payload_length += payload_length;

  return TRUE;
}

static void
reset_payload (GstSCTE104PacketParser * parser)
{
  parser->payload_length = 0;
}

static gboolean
continued_payload (GstSCTE104PacketParser * parser,
    const guint8 * payload, guint16 payload_length)
{
  /* State machine has been reset before entering here.
   * Go ahead and append the first header into the fragment list.
   */
  return append_payload (parser, payload, payload_length);
}

static gboolean
following_payload (GstSCTE104PacketParser * parser,
    const guint8 * payload, guint16 payload_length)
{
  /* A following packet must have been proceeded with one or more previous packets. */
  if (parser->payload_length == 0) {
    /* Possible stream corruption causing a missing continuation packet. */
    GST_ERROR
        ("SCTE104, Following wasn't proceeded with a Continued. Skipping");
    return FALSE;
  }

  /* Thinks appear ok, go ahead and append. */
  return append_payload (parser, payload, payload_length);
}

static guint16
final_payload (GstSCTE104PacketParser * parser,
    const guint8 * payload, guint16 payload_length)
{
  guint16 ret;

  /* A final packet must have been proceeded with one or more previous packets. */
  if (parser->payload_length == 0) {
    /* Possible stream corruption causing a missing continuation packet. */
    GST_ERROR
        ("SCTE104, Final wasn't proceeded with a Continued. Resetting statemachine.");
    return 0;
  }

  if (!append_payload (parser, payload, payload_length)) {
    /* Possible stream corruption causing a missing continuation packet. */
    GST_ERROR ("SCTE104, Final unable to append. Resetting statemachine.");
    reset_payload (parser);
    return -1;
  }

  ret = parser->payload_length;

  /* We're done, reset */
  reset_payload (parser);

  return ret;                   /* Success */
}

gboolean
gst_scte_104_packet_parser_parse (GstSCTE104PacketParser * parser,
    GstSCTE104Packet * pkt, const guint8 * payload, guint16 payload_length)
{
  /* TODO: GstBitReader, bit access is quite unsafe */
  /* See See ST2010-2008 Section 5.1 UDW Format */
  pkt->payload_descriptor_byte = payload[0];
  payload += 1;
  payload_length -= 1;

  pkt->version = (pkt->payload_descriptor_byte >> 3) & 0x03;
  pkt->continued_pkt = (pkt->payload_descriptor_byte >> 2) & 0x01;
  pkt->following_pkt = (pkt->payload_descriptor_byte >> 1) & 0x01;
  pkt->duplicate_msg = pkt->payload_descriptor_byte & 0x01;

  /* We only support SCTE104 messages of type
   * single_operation_message() that are completely
   * self containined with a single VANC line, and
   * are not continuation messages.
   * Eg. payloadDescriptor value 0x08.
   *
   * Duplicate message flags are not supported.
   */

  if (pkt->duplicate_msg) {
    GST_ERROR ("pkt->duplicate_msg is unsupported, parse aborted.");
    return FALSE;
  }

  if (pkt->payload_descriptor_byte != 0x08) {
    /* Process one or more messages in fragmented form */

    /* See ST2010-2008 Table 3 - Continued Packet and Following Packet Flag Bits */
    if (pkt->continued_pkt && pkt->following_pkt == 0) {
      /* First packet.
       * Begin of a new series of fragmented packets.
       * Handle any previous allocations, if packet loss or issues caused
       * The fragmented state machine to become broken.
       */
      reset_payload (parser);
      continued_payload (parser, payload, payload_length);
      return FALSE;             /* Signal upper layers we're not happy. In reality we're collecting. */
    } else if (pkt->continued_pkt && pkt->following_pkt) {
      /* Intermediate packet */
      following_payload (parser, payload, payload_length);
      return FALSE;             /* Signal upper layers we're not happy. In reality we're collecting. */
    } else if (pkt->continued_pkt == 0 && pkt->following_pkt) {
      /* Final packet */
      if ((payload_length =
              final_payload (parser, payload, payload_length) == 0)) {
        GST_ERROR ("unable to assemble fragments, skipping.");
        return FALSE;
      }
      /* Use the complete defragged header, not the final fragment header
       * in all the following parsing.
       */
      payload = parser->payload;
    } else {
      GST_ERROR ("pkt->payload_descriptor_byte != 0x08 (0x%x)",
          pkt->payload_descriptor_byte);
      return FALSE;
    }
  } else {
    /* Process a single complete message inside this hdr packet. */

    /* Avoid cases where we're mixing single messages potentially when in the process
     * of message fragment building. Lose any previous fragments
     */
    reset_payload (parser);
  }

  /* SCTE104 packets can be 200 bytes (single message) and up to 2000 bytes
   * in length (ST2010-2008 section 5) for multiple messages fragmented.
   */

  GstSCTE104SingleOperation *m = &pkt->so_msg;
  GstSCTE104MultipleOperation *mom = &pkt->mo_msg;

  /* Make sure we put the op_id in the SOM reegardless of
   * whether the message ius single or multiple.
   * We rely on checking som.op_id during the dump process
   * to determinate the structure type.
   */
  m->op_id = payload[0] << 8 | payload[1];

#ifdef SPLICE_REQUEST_SINGLE
  if (m->op_id == SO_INIT_REQUEST_DATA) {

    /* TODO: Will we ever see a trigger in a SOM. Interal discussion says
     *       no. We'll leave this active for the time being, pending removal.
     */
    m->message_size = payload[2] << 8 | payload[3];
    m->result = payload[4] << 8 | payload[5];
    m->result_extension = payload[6] << 8 | payload[7];
    m->protocol_version = payload[8];
    m->as_index = payload[9];
    m->message_number = payload[10];
    m->dpi_pid_index = payload[11] << 8 | payload[12];

    GstSCTE104SpliceRequestData *d = &pkt->sr_data;

    d->splice_insert_type = payload[13];
    d->splice_event_id = payload[14] << 24 |
        payload[15] << 16 | payload[16] << 8 | payload[17];
    d->unique_program_id = payload[18] << 8 | payload[19];
    d->pre_roll_time = payload[20] << 8 | payload[21];
    d->brk_duration = payload[22] << 8 | payload[23];
    d->avail_num = payload[24];
    d->avails_expected = payload[25];
    d->auto_return_flag = payload[26];

    /* TODO: We only support spliceStart_immediate and spliceEnd_immediate */
    switch (d->splice_insert_type) {
      case SPLICESTART_IMMEDIATE:
      case SPLICEEND_IMMEDIATE:
        break;
      default:
        /* We don't support this splice command */
        GST_ERROR ("splice_insert_type 0x%x", d->splice_insert_type);
        return FALSE;
    }
  } else
#endif
  if (m->op_id == 0xFFFF /* Multiple Operation Message */ ) {
    mom->rsvd = payload[0] << 8 | payload[1];
    mom->message_size = payload[2] << 8 | payload[3];
    mom->protocol_version = payload[4];
    mom->as_index = payload[5];
    mom->message_number = payload[6];
    mom->dpi_pid_index = payload[7] << 8 | payload[8];
    mom->scte35_protocol_version = payload[9];

    const guint8 *p = &payload[10];
    p = parse_mom_timestamp (p, &mom->timestamp);

    mom->num_ops = *(p++);
    mom->ops =
        calloc (mom->num_ops, sizeof (GstSCTE104MultipleOperationOperation));

    for (guint i = 0; i < mom->num_ops; i++) {
      GstSCTE104MultipleOperationOperation *o = &mom->ops[i];
      o->op_id = *(p + 0) << 8 | *(p + 1);
      o->data_length = *(p + 2) << 8 | *(p + 3);
      o->data = malloc (o->data_length);
      memcpy (o->data, p + 4, o->data_length);
      p += (4 + o->data_length);

      if (o->op_id == MO_SPLICE_REQUEST_DATA)
        parse_splice_request_data (o->data, &o->sr_data);
      else if (o->op_id == MO_TIME_SIGNAL_REQUEST_DATA)
        parse_time_signal_request_data (o->data, &o->timesignal_data);
      else if (o->op_id == MO_INSERT_DESCRIPTOR_REQUEST_DATA)
        parse_descriptor_request_data (o->data, &o->descriptor_data,
            o->data_length - 1);
      else if (o->op_id == MO_INSERT_AVAIL_DESCRIPTOR_REQUEST_DATA)
        parse_avail_request_data (o->data, &o->avail_descriptor_data);
      else if (o->op_id == MO_INSERT_DTMF_REQUEST_DATA)
        parse_dtmf_request_data (o->data, &o->dtmf_data);
      else if (o->op_id == MO_INSERT_SEGMENTATION_REQUEST_DATA)
        parse_segmentation_request_data (o->data, &o->segmentation_data);
      else if (o->op_id == MO_PROPRIETARY_COMMAND_REQUEST_DATA)
        parse_proprietary_command_request_data (o->data, &o->proprietary_data,
            o->data_length);
      else if (o->op_id == MO_INSERT_TIER_DATA)
        parse_tier_data (o->data, &o->tier_data);
      else if (o->op_id == MO_INSERT_TIME_DESCRIPTOR)
        parse_time_descriptor (o->data, &o->time_data);
    }

    /* We'll parse this message but we'll only look for INIT_REQUEST_DATA
     * sub messages, and construct a splice_request_data message.
     * The rest of the message types will be ignored.
     */
  } else {
    GST_ERROR ("Unsupported opID = %x, error.", m->op_id);
    return FALSE;
  }

  return TRUE;
}

GstSCTE104PacketParser *
gst_scte_104_packet_parser_new (void)
{
  return g_malloc0 (sizeof (GstSCTE104PacketParser));
}

void
gst_scte_104_packet_parser_free (GstSCTE104PacketParser * parser)
{
  reset_payload (parser);
  g_free (parser);
}

static void
gen_multiple_operation_operation (const GstSCTE104MultipleOperationOperation *
    o, GstBitWriter * bw)
{
  guint16 size;

  switch (o->op_id) {
    case MO_SPLICE_REQUEST_DATA:
      size = 14;
      break;
    case MO_SPLICE_NULL_REQUEST_DATA:
      size = 0;
      break;
    case MO_TIME_SIGNAL_REQUEST_DATA:
      size = 2;
      break;
    case MO_INSERT_DESCRIPTOR_REQUEST_DATA:
      size = 1 + o->descriptor_data.total_length;
      break;
    case MO_INSERT_DTMF_REQUEST_DATA:
      size = 2 + o->dtmf_data.dtmf_length;
      break;
    case MO_INSERT_AVAIL_DESCRIPTOR_REQUEST_DATA:
      size = 1 + o->avail_descriptor_data.num_provider_avails * 4;
      break;
    case MO_INSERT_SEGMENTATION_REQUEST_DATA:
      size = 18 + o->segmentation_data.upid_length;
      break;
    case MO_PROPRIETARY_COMMAND_REQUEST_DATA:
      size = 5 + o->proprietary_data.data_length;
      break;
    case MO_INSERT_TIER_DATA:
      size = 2;
      break;
    case MO_INSERT_TIME_DESCRIPTOR:
      size = 12;
      break;
    default:
      GST_ERROR ("Unknown operation type 0x%04x", o->op_id);
      return;
  }

  gst_bit_writer_put_bits_uint16 (bw, o->op_id, 16);
  gst_bit_writer_put_bits_uint16 (bw, size, 16);

  switch (o->op_id) {
    case MO_SPLICE_REQUEST_DATA:
      gen_splice_request_data (&o->sr_data, bw);
      break;
    case MO_SPLICE_NULL_REQUEST_DATA:
      /* Nothing to do */
      break;
    case MO_TIME_SIGNAL_REQUEST_DATA:
      gen_time_signal_request_data (&o->timesignal_data, bw);
      break;
    case MO_INSERT_DESCRIPTOR_REQUEST_DATA:
      gen_descriptor_request_data (&o->descriptor_data, bw);
      break;
    case MO_INSERT_DTMF_REQUEST_DATA:
      gen_dtmf_request_data (&o->dtmf_data, bw);
      break;
    case MO_INSERT_AVAIL_DESCRIPTOR_REQUEST_DATA:
      gen_avail_request_data (&o->avail_descriptor_data, bw);
      break;
    case MO_INSERT_SEGMENTATION_REQUEST_DATA:
      gen_segmentation_request_data (&o->segmentation_data, bw);
      break;
    case MO_PROPRIETARY_COMMAND_REQUEST_DATA:
      gen_proprietary_command_request_data (&o->proprietary_data, bw);
      break;
    case MO_INSERT_TIER_DATA:
      gen_tier_data (&o->tier_data, bw);
      break;
    case MO_INSERT_TIME_DESCRIPTOR:
      gen_time_descriptor (&o->time_data, bw);
      break;
    default:
      g_assert_not_reached ();
  }
}

gboolean
gst_scte_104_packet_to_bytes (const GstSCTE104Packet * pkt, guint8 ** bytes,
    guint16 * byte_count)
{
  const GstSCTE104MultipleOperation *m;
  GstBitWriter bw;

  if (pkt->so_msg.op_id != 0xffff) {
    /* We don't currently support anything but Multiple Operation
       Messages */
    GST_ERROR ("msg opid not 0xffff.  Provided=0x%x", pkt->so_msg.op_id);
    return FALSE;
  }

  gst_bit_writer_init_with_size (&bw, 2000, FALSE);

  m = &pkt->mo_msg;

  gst_bit_writer_put_bits_uint16 (&bw, 0xffff, 16);     /* reserved */

  gst_bit_writer_put_bits_uint16 (&bw, m->message_size, 16);
  gst_bit_writer_put_bits_uint8 (&bw, m->protocol_version, 8);
  gst_bit_writer_put_bits_uint8 (&bw, m->as_index, 8);
  gst_bit_writer_put_bits_uint8 (&bw, m->message_number, 8);
  gst_bit_writer_put_bits_uint16 (&bw, m->dpi_pid_index, 16);
  gst_bit_writer_put_bits_uint8 (&bw, m->scte35_protocol_version, 8);
  gst_bit_writer_put_bits_uint8 (&bw, m->timestamp.time_type, 8);

  const GstSCTE104MultipleOperationTimestamp *ts = &m->timestamp;
  switch (ts->time_type) {
    case 1:
      gst_bit_writer_put_bits_uint32 (&bw, ts->time_type_1.utc_seconds, 32);
      gst_bit_writer_put_bits_uint16 (&bw, ts->time_type_1.utc_microseconds,
          16);
      break;
    case 2:
      gst_bit_writer_put_bits_uint8 (&bw, ts->time_type_2.hours, 8);
      gst_bit_writer_put_bits_uint8 (&bw, ts->time_type_2.minutes, 8);
      gst_bit_writer_put_bits_uint8 (&bw, ts->time_type_2.seconds, 8);
      gst_bit_writer_put_bits_uint8 (&bw, ts->time_type_2.frames, 8);
      break;
    case 3:
      gst_bit_writer_put_bits_uint8 (&bw, ts->time_type_3.gpi_number, 8);
      gst_bit_writer_put_bits_uint8 (&bw, ts->time_type_3.gpi_edge, 8);
      break;
    case 0:
      /* No time standard defined */
      break;
    default:
      GST_ERROR ("unsupported time_type 0x%x, assuming no time.",
          ts->time_type);
      break;
  }

  gst_bit_writer_put_bits_uint8 (&bw, m->num_ops, 8);

  for (guint i = 0; i < m->num_ops; i++) {
    const GstSCTE104MultipleOperationOperation *o = &m->ops[i];

    gen_multiple_operation_operation (o, &bw);
  }

  /* Recompute the total message size now that everything has been serialized to
     a single buffer. */
  gst_bit_writer_align_bytes (&bw, 0);
  *byte_count = gst_bit_writer_get_size (&bw) / 8;
  *bytes = gst_bit_writer_reset_and_get_data (&bw);
  (*bytes)[2] = (*byte_count) >> 8;
  (*bytes)[3] = (*byte_count) & 0xff;

  return TRUE;
}

gboolean
gst_scte_104_packet_add_multiple_operation_operation (GstSCTE104Packet * pkt,
    guint16 op_id, GstSCTE104MultipleOperationOperation ** op)
{
  GstSCTE104MultipleOperation *mom = &pkt->mo_msg;
  mom->num_ops++;
  mom->ops = realloc (mom->ops,
      mom->num_ops * sizeof (GstSCTE104MultipleOperationOperation));
  *op = &mom->ops[mom->num_ops - 1];
  memset (*op, 0, sizeof (GstSCTE104MultipleOperationOperation));
  (*op)->op_id = op_id;

  return TRUE;
}

/* SCTE 104 meta */

GType
gst_scte_104_meta_api_get_type (void)
{
  static GType type = 0;

  if (g_once_init_enter (&type)) {
    static const gchar *tags[] = { NULL };
    GType _type = gst_meta_api_type_register ("GstSCTE104MetaAPI", tags);
    g_once_init_leave (&type, _type);
  }
  return type;
}

static gboolean
gst_scte_104_meta_transform (GstBuffer * dest, GstMeta * meta,
    GstBuffer * buffer, GQuark type, gpointer data)
{
  GstSCTE104Meta *dmeta, *smeta;
  guint8 *data_copy;

  /* We always copy over the SCTE 104 meta */
  smeta = (GstSCTE104Meta *) meta;

  data_copy = g_malloc (smeta->size);
  memcpy (data_copy, smeta->data, smeta->size);

  dmeta = gst_buffer_add_scte_104_meta (dest, data_copy, smeta->size);

  if (!dmeta)
    return FALSE;

  return TRUE;
}

static gboolean
gst_scte_104_meta_init (GstMeta * meta, gpointer params, GstBuffer * buffer)
{
  GstSCTE104Meta *emeta = (GstSCTE104Meta *) meta;

  emeta->data = NULL;
  emeta->size = 0;

  return TRUE;
}

static void
gst_scte_104_meta_free (GstMeta * meta, GstBuffer * buffer)
{
  GstSCTE104Meta *emeta = (GstSCTE104Meta *) meta;

  g_free (emeta->data);
}

const GstMetaInfo *
gst_scte_104_meta_get_info (void)
{
  static const GstMetaInfo *meta_info = NULL;

  if (g_once_init_enter ((GstMetaInfo **) & meta_info)) {
    const GstMetaInfo *mi = gst_meta_register (GST_SCTE_104_META_API_TYPE,
        "GstSCTE104Meta",
        sizeof (GstSCTE104Meta),
        gst_scte_104_meta_init,
        gst_scte_104_meta_free,
        gst_scte_104_meta_transform);
    g_once_init_leave ((GstMetaInfo **) & meta_info, (GstMetaInfo *) mi);
  }
  return meta_info;
}

/**
 * gst_buffer_add_scte_104_meta:
 * @buffer: a #GstBuffer
 * @data: (array length=size) (transfer full): The SCTE 104 data
 * @size: The size of @data in bytes
 *
 * Attaches #GstSCTE104Meta metadata to @buffer
 *
 * Returns: (transfer none): the #GstSCTE104Meta on @buffer.
 *
 * Since: 1.24
 */
GstSCTE104Meta *
gst_buffer_add_scte_104_meta (GstBuffer * buffer, guint8 * data, gsize size)
{
  GstSCTE104Meta *meta;

  g_return_val_if_fail (GST_IS_BUFFER (buffer), NULL);
  g_return_val_if_fail (data != NULL, NULL);
  g_return_val_if_fail (size > 0, NULL);

  meta = (GstSCTE104Meta *) gst_buffer_add_meta (buffer,
      GST_SCTE_104_META_INFO, NULL);
  g_return_val_if_fail (meta != NULL, NULL);

  meta->data = data;
  meta->size = size;

  return meta;
}
