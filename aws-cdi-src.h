/* GStreamer
 *
 * Copyright (C) 2022 Mathieu Duponchelle <mathieu@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_AWS_CDI_SRC_H__
#define __GST_AWS_CDI_SRC_H__

#include <gst/gst.h>
#include <gst/base/base.h>
#include <cdi_core_api.h>

G_BEGIN_DECLS

/* Inner element */

#define GST_TYPE_AWS_CDI_SRC_INNER \
  (gst_aws_cdi_src_inner_get_type())
#define GST_AWS_CDI_SRC_INNER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_AWS_CDI_SRC_INNER, GstAwsCdiSrcInner))
#define GST_AWS_CDI_SRC_INNER_CAST(obj) \
  ((GstAwsCdiSrcInner*)obj)
#define GST_AWS_CDI_SRC_INNER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_AWS_CDI_SRC_INNER, GstAwsCdiSrcInnerClass))
#define GST_IS_AWS_CDI_SRC_INNER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_AWS_CDI_SRC_INNER))
#define GST_IS_AWS_CDI_SRC_INNER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_AWS_CDI_SRC_INNER))

typedef struct _GstAwsCdiSrcInner GstAwsCdiSrcInner;
typedef struct _GstAwsCdiSrcInnerClass GstAwsCdiSrcInnerClass;

struct _GstAwsCdiSrcInner
{
  GstPushSrc parent;

  GCond cond;

  gboolean unlock;

  GstQueueArray *input_queue;

  /* Set from the RX callback, governs whether we will
   * discard data before receiving a configuration.
   */
  GstCaps *received_config;

  /* Only set at construction */
  gboolean is_video;
};

struct _GstAwsCdiSrcInnerClass
{
  GstPushSrcClass parent_class;
};

GType gst_aws_cdi_src_inner_get_type (void);

/* Outer bin */

#define GST_TYPE_AWS_CDI_SRC \
  (gst_aws_cdi_src_get_type())
#define GST_AWS_CDI_SRC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_AWS_CDI_SRC, GstAwsCdiSrc))
#define GST_AWS_CDI_SRC_CAST(obj) \
  ((GstAwsCdiSrc*)obj)
#define GST_AWS_CDI_SRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_AWS_CDI_SRC, GstAwsCdiSrcClass))
#define GST_IS_AWS_CDI_SRC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_AWS_CDI_SRC))
#define GST_IS_AWS_CDI_SRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_AWS_CDI_SRC))

typedef struct _GstAwsCdiSrc GstAwsCdiSrc;
typedef struct _GstAwsCdiSrcClass GstAwsCdiSrcClass;

struct _GstAwsCdiSrc
{
  GstBin parent;

  CdiConnectionHandle connection_handle;
  CdiAdapterHandle adapter_handle;
  CdiConnectionStatus connection_status;

  gboolean stopping;

  gint meta_stream;
  GHashTable *inner_sources;

  GstQueueArray *vanc_queue;

  /* properties */
  gint port;
  gchar *address;
};

struct _GstAwsCdiSrcClass
{
  GstBinClass parent_class;
};

GType gst_aws_cdi_src_get_type (void);

GST_ELEMENT_REGISTER_DECLARE (aws_cdi_src);

G_END_DECLS

#endif /* __GST_AWS_CDI_SRC_H__ */
